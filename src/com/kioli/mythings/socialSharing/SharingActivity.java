package com.kioli.mythings.socialSharing;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

public class SharingActivity extends BetterActivity {

	Context context;
	String text_to_share = "Hey There!";
	int image_to_share = R.drawable.ic_launcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_sharing);
		init();
	}

	private void init() {
		findViewById(R.id.btn_sharing).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SharingUtil.shareDrawableResource(context, image_to_share, text_to_share);
			}
		});
	}
}