package com.kioli.mythings.socialSharing;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import com.kioli.mythings.utils.MyLog;

/** Utility class for easy sharing different things by ACTION_SEND Intent */
public class SharingUtil {

	private static Intent intent;

	/** Share a single line of text, using the default chooser
	 *
	 * @param ctx
	 *        Context used to startActivity()
	 * @param text
	 *        Text to share */
	public synchronized static void share(Context ctx, String text) {

		intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, text);

		ctx.startActivity(intent);
	}

	/** Share a single line of text, with a specified chooser text
	 *
	 * @param ctx
	 *        Context used to startActivity()
	 * @param chooser_text
	 *        Text to display on the chooser
	 * @param text
	 *        Text to share */
	public synchronized static void share(Context ctx, String chooser_text, String text) {

		intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, text);

		ctx.startActivity(Intent.createChooser(intent, chooser_text));
	}

	/** Saves a Drawable from the app's resources to local storage, and shares it. This method should pick the HDPI
	 * version where available.
	 *
	 * @param ctx
	 *        Context used to startActivity()
	 * @param drawable_id
	 *        ID of the Drawable to share
	 * @param text
	 *        Text to add to the image
	 * @param alreadySent
	 *        Boolean to check whether this image was already sent before */
	public synchronized static void shareDrawableResource(Context ctx, int drawable_id, String text) {

		// Construct custom Resources instance to assure HDPI drawables are fetched
		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
		metrics.densityDpi = DisplayMetrics.DENSITY_HIGH;
		Resources res = new Resources(ctx.getAssets(), metrics, ctx.getResources().getConfiguration());

		/** Open an inputstream and fetch some bytes from the raw resource */
		InputStream inputStream = res.openRawResource(drawable_id);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		int size = 0;
		String path = "";
		String imageUriString = "";

		// Read the entire resource into a local byte buffer.
		byte[] buffer = new byte[1024];

		try {
			while ((size = inputStream.read(buffer, 0, 1024)) >= 0) {
				outputStream.write(buffer, 0, size);
			}

			inputStream.close();
			buffer = outputStream.toByteArray();
			path = ctx.getCacheDir() + res.getResourceEntryName(drawable_id) + ".png";

			// Write the thing back, to the app's cachedir
			FileOutputStream fos = new FileOutputStream(path);
			fos.write(buffer);
			fos.close();

			// Write the thing to publicly available storage (the MediaStore)
			// TODO find a better way, or at least prevent multiple copies of the same image
			imageUriString = MediaStore.Images.Media.insertImage(ctx.getContentResolver(), path, res.getResourceEntryName(drawable_id), null);
		} catch (Exception e) {
			MyLog.e("SharingUtil", "Exception reading/writing image: " + e);
			MyLog.printStackTrace("SharingUtil", e);
		}

		// finally, action!
		intent = new Intent(Intent.ACTION_SEND);
		intent.setType("image/png");
		intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(imageUriString));
		intent.putExtra(Intent.EXTRA_TEXT, text);

		ctx.startActivity(intent);
	}
}