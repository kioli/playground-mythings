package com.kioli.mythings.stringStyle;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

public class StringStyleActivity extends BetterActivity {

	private int number_of_clicks = 0;
	private TextView text_times;

	private int name_index = 0;
	private TextView text_name;
	private String[] names;
	private int day;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_string_style);
		init();
	}

	private void init() {
		findViewById(R.id.btn_increment).setOnClickListener(listener);
		findViewById(R.id.btn_change_name).setOnClickListener(listener);

		text_name = (TextView) findViewById(R.id.string_styled_text1);
		text_times = (TextView) findViewById(R.id.string_styled_text2);

		names = getResources().getStringArray(R.array.names_array);
		GregorianCalendar calendar = new GregorianCalendar();
		day = calendar.get(Calendar.DAY_OF_MONTH);

		text_name.setText(getResources().getString(R.string.stringStyle_txt, names[0], day));
		text_times.setText(MessageFormat.format(getResources().getString(R.string.number_of_clicks), number_of_clicks));
	}

	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.btn_increment:
					number_of_clicks++;
					if (number_of_clicks > 10) {
						number_of_clicks = 0;
					}
					text_times.setText(MessageFormat.format(getResources().getString(R.string.number_of_clicks), number_of_clicks));
					break;
				case R.id.btn_change_name:
					name_index++;
					if (name_index >= names.length) {
						name_index=0;
					}
					text_name.setText(getResources().getString(R.string.stringStyle_txt, names[name_index], day));
					break;
				default:
					break;
			}
		}
	};
}