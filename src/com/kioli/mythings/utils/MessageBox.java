package com.kioli.mythings.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.TextUtils;

/**
 * Created by: Kioli Mikontalo
 * Date: 06/10/13
 */
public class MessageBox extends DialogFragment implements DialogInterface.OnClickListener {

    private static final int INDEX_POSITIVE = 0;
    private static final int INDEX_NEUTRAL  = 1;
    private static final int INDEX_NEGATIVE = 2;

    public interface OnDialogFinish {
        abstract void onDialogFinish(DIALOG_BUTTON buttonClicked);
    }

    /**
     * Buttons are used in OnDialogFinish for determining
     * which button has been pressed
     */
    public enum DIALOG_BUTTON {
        OK        (android.R.string.ok),
        CANCEL    (android.R.string.cancel),
        YES       (android.R.string.yes),
        NO        (android.R.string.no),
		/* Add more buttons here */
        ;

        public final int captionId;

        private DIALOG_BUTTON(final int captionId) {
            this.captionId = captionId;
        }
    }

    /**
     * Buttons used in MessageBox
     */
    public enum DIALOG_BUTTON_TYPE {
        /*                 positive                neutral              negative  */
        OK                (DIALOG_BUTTON.OK,       null,                null),
        CANCEL            (DIALOG_BUTTON.CANCEL,   null,                null),
        OK_CANCEL         (DIALOG_BUTTON.OK,       null,                DIALOG_BUTTON.CANCEL),
        YES_NO            (DIALOG_BUTTON.YES,      null,                DIALOG_BUTTON.NO),
        YES_NO_CANCEL     (DIALOG_BUTTON.YES,      DIALOG_BUTTON.NO,    DIALOG_BUTTON.CANCEL),
        CANCEL_YES        (DIALOG_BUTTON.CANCEL,   null,                DIALOG_BUTTON.YES),
		/* Add more combinations here as you need */
        ;

        public final DIALOG_BUTTON[] buttons;

        private DIALOG_BUTTON_TYPE(final DIALOG_BUTTON pos, final DIALOG_BUTTON neut, final DIALOG_BUTTON neg) {
            buttons = new DIALOG_BUTTON[3];
            buttons[INDEX_POSITIVE] = pos;
            buttons[INDEX_NEUTRAL]  = neut;
            buttons[INDEX_NEGATIVE] = neg;
        }
    }

    private AlertDialog.Builder builder;
    private DIALOG_BUTTON[]     mButtons;
    private OnDialogFinish onDialogFinish;

    /** Dialog with personalized message */
    public MessageBox(final Context context, final String message) {
        init(context);
        create(message, null, DIALOG_BUTTON_TYPE.OK, false);
    }

    public MessageBox(final Context context, final int messageResourceId) {
        init(context);
        create(context.getString(messageResourceId), null, DIALOG_BUTTON_TYPE.OK, false);
    }

    /** Dialog with personalized message and title */
    public MessageBox(final Context context, final String message, final String title) {
        init(context);
        create(message, title, DIALOG_BUTTON_TYPE.OK, false);
    }

    public MessageBox(final Context context, final int messageResourceId, final int titleResourceId) {
        init(context);
        create(context.getString(messageResourceId), context.getString(titleResourceId), DIALOG_BUTTON_TYPE.OK, false);
    }

    public MessageBox(final Context context, final String message, final int titleResourceId) {
        init(context);
        create(message, context.getString(titleResourceId), DIALOG_BUTTON_TYPE.OK, false);
    }

    public MessageBox(final Context context, final int messageResourceId, final String title) {
        init(context);
        create(context.getString(messageResourceId), title, DIALOG_BUTTON_TYPE.OK, false);
    }


    /** Dialog with personalized message, title and buttons*/
    public MessageBox(final Context context, final String message, final String title, final DIALOG_BUTTON_TYPE buttons) {
        init(context);
        create(message, title, buttons, false);
    }

    public MessageBox(final Context context, final int messageResourceId, final int titleResourceId, final DIALOG_BUTTON_TYPE buttons) {
        init(context);
        create(context.getString(messageResourceId), context.getString(titleResourceId), buttons,false);
    }

    public MessageBox(final Context context, final String message, final int titleResourceId, final DIALOG_BUTTON_TYPE buttons) {
        init(context);
        create(message, context.getString(titleResourceId), buttons,false);
    }

    public MessageBox(final Context context, final int messageResourceId, final String title, final DIALOG_BUTTON_TYPE buttons) {
        init(context);
        create(context.getString(messageResourceId), title, buttons,false);
    }

    public MessageBox(final Context context, final int messageResourceId, final int titleResourceId, final DIALOG_BUTTON_TYPE buttons, final boolean showAlert){
        init(context);
        create(context.getString(messageResourceId), context.getString(titleResourceId), buttons, showAlert);
    }

    public MessageBox(final Context context, final String message, final int titleResourceId, final DIALOG_BUTTON_TYPE buttons, final boolean showAlert){
        init(context);
        create(message, context.getString(titleResourceId), buttons, showAlert);
    }

    private void init(final Context context) {
        builder = new AlertDialog.Builder(context);
        setCancelable(false);
    }

    /** Creates the dialog */
    private void create(final String message, final String title, final DIALOG_BUTTON_TYPE buttons, final boolean showAlertIcon) {
        builder.setMessage(Html.fromHtml(message));

        if (TextUtils.isEmpty(title) == false) {
            builder.setTitle(title);
        }

        mButtons = buttons.buttons;

        if (mButtons[INDEX_POSITIVE] != null) { builder.setPositiveButton(mButtons[INDEX_POSITIVE].captionId, this); }
        if (mButtons[INDEX_NEUTRAL]  != null) { builder.setNeutralButton (mButtons[INDEX_NEUTRAL].captionId,  this); }
        if (mButtons[INDEX_NEGATIVE] != null) { builder.setNegativeButton(mButtons[INDEX_NEGATIVE].captionId, this); }

        if (showAlertIcon) {
            builder.setIcon(android.R.drawable.ic_dialog_alert);
        }
    }

    public void setCancelable(final boolean cancelable) {
        builder.setCancelable(cancelable);
    }

    public Dialog create() {
        return builder.create();
    }

    public void show() {
        create().show();
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        final DIALOG_BUTTON btn = decodeButton(which);
        onDialogFinish(btn);
    }

    protected DIALOG_BUTTON decodeButton(final int which) {
        final int ind = decodeIndex(which);
        return mButtons[ind];
    }

    private int decodeIndex(final int which) {
        switch (which) {
            case AlertDialog.BUTTON_POSITIVE: return INDEX_POSITIVE;
            case AlertDialog.BUTTON_NEUTRAL:  return INDEX_NEUTRAL;
            case AlertDialog.BUTTON_NEGATIVE: return INDEX_NEGATIVE;
            default:                          throw new IllegalArgumentException();
        }
    }

    /** Set the callback. This callback is called when a button is pressed */
    public void setOnDialogFinish(final OnDialogFinish onDialogFinish) {
        this.onDialogFinish = onDialogFinish;
    }

    protected void onDialogFinish(final DIALOG_BUTTON button) {
        if(onDialogFinish != null) {
            onDialogFinish.onDialogFinish(button);
        }
    }
}