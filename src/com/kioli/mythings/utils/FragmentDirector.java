package com.kioli.mythings.utils;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public abstract class FragmentDirector<E extends Enum<?>, A extends FragmentActivity> extends FrameLayout {
    protected A mActivity;

    @SuppressWarnings("unchecked")
    public FragmentDirector(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        if (context instanceof FragmentActivity) {
            mActivity = (A) context;
        } else {
            throw new IllegalStateException("FragmentDirector can work only inside a FragmentActivity");
        }
    }

    protected FragmentManager getFragmentManager() {
        if (mActivity != null) {
            return mActivity.getSupportFragmentManager();
        } else {
            return null;
        }
    }

    abstract public void onResume();
    abstract public boolean goBack();
    abstract public void forwardWithFragment(final E fragm);
}