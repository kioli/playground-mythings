package com.kioli.mythings.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import android.app.Activity;
import android.content.Context;

/** Used to read and write Objects from/to the app's private local storage */
public class LocalPersistence {

	/** Writes an Object to storage
	 *
	 * @param context
	 *        Context used to get access to storage
	 * @param object
	 *        Object to be written (must implement Serializable)
	 * @param filename
	 *        String filename to us
	 **/
	public static void writeObjectToFile(Context context, Object object, String filename) {

		ObjectOutputStream objectOut = null;

		try {

			FileOutputStream fileOut = context.openFileOutput(filename, Activity.MODE_PRIVATE);
			objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(object);
			fileOut.getFD().sync();

		} catch (Exception e) {
			MyLog.printStackTrace("LocalPersistence.writeObjectToFile", e);
		} finally {
			if (objectOut != null) {
				try {
					objectOut.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/** Reads a previously written Object from storage
	 *
	 * @param context
	 *        Context used to get access to storage
	 * @param filename
	 *        String filename to use
	 * @return Object read from storage
	 **/
	public static Object readObjectFromFile(Context context, String filename) {

		ObjectInputStream objectIn = null;
		Object object = null;
		try {

			FileInputStream fileIn = context.getApplicationContext().openFileInput(filename);
			objectIn = new ObjectInputStream(fileIn);
			object = objectIn.readObject();

		} catch (FileNotFoundException e) {
			// do nothing here - likely to occur on first run
		} catch (Exception e) {
			MyLog.printStackTrace("LocalPersistence.readObjectFromFile", e);
		} finally {
			if (objectIn != null) {
				try {
					objectIn.close();
				} catch (Exception e) {
					MyLog.printStackTrace("LocalPersistence", e);
				}
			}
		}
		return object;
	}
}