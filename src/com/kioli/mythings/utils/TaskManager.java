package com.kioli.mythings.utils;

import java.util.LinkedList;

/** Class to handle background tasks */
public class TaskManager {

	private static LinkedList<Runnable> tasks_list;
	private static Thread thread;
	private static boolean is_running;
	static InternalRunnable internalRunnable;

	/** Constructor of task manager */
	TaskManager() {
		TaskManager.tasks_list = new LinkedList<Runnable>();
		TaskManager.internalRunnable = new InternalRunnable();
		TaskManager.is_running = false;
		MyLog.d("TaskManager", "Created");
	}

	/** Initializes task manager */
	static void start() {
		if (!TaskManager.is_running) {
			TaskManager.thread = new Thread(TaskManager.internalRunnable);
			TaskManager.thread.setDaemon(true);
			TaskManager.is_running = true;
			TaskManager.thread.start();
			MyLog.d("TaskManager", "Started");
		}
	}

	/** Stops task manager */
	static void stop() {
		TaskManager.is_running = false;
		MyLog.d("TaskManager", "Stopped");
	}

	/** @param task
	 *        Add new task to the queue */
	public static void addTask(final Runnable task) {
		synchronized (TaskManager.tasks_list) {
			TaskManager.tasks_list.addFirst(task);
			// notify any waiting threads
			TaskManager.tasks_list.notify();
			MyLog.d("TaskManager", "TaskAdded");
		}
	}

	/** @param task
	 *        Function used to add a task that should be run first asap. */
	static void addPriorityTask(final Runnable task) {
		synchronized (TaskManager.tasks_list) {
			TaskManager.tasks_list.addLast(task);
			// notify any waiting threads
			TaskManager.tasks_list.notify();
			MyLog.d("TaskManager", "BlockingTaskAdded");
		}
	}

	/** Extract the next task to be run from the list if there's any, otherwise waits for it */
	private Runnable getNextTask() {
		MyLog.d("TaskManager", "getNextTask");
		synchronized (TaskManager.tasks_list) {
			if (TaskManager.tasks_list.isEmpty()) {
				try {
					MyLog.v("TaskManager", "Waiting");
					TaskManager.tasks_list.wait();
				} catch (final InterruptedException e) {
					MyLog.e("TaskManager", "Task interrupted", e);
					TaskManager.stop();
				}
			}
			return TaskManager.tasks_list.removeLast();
		}
	}

	void internalRun() {
		while (TaskManager.is_running) {
			final Runnable task = this.getNextTask();
			try {
				MyLog.v("TaskManager", "RunTask");
				task.run();
			} catch (final Throwable t) {
				MyLog.e("TaskManager", "Task threw an exception", t);
			}
		}
	}

	/** Class for runnables that run checking if the class is running and getting a task from its list */
	class InternalRunnable implements Runnable {
		@Override
		public void run() {
			TaskManager.this.internalRun();
		}
	}
}