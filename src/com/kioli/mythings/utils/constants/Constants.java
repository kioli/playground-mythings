package com.kioli.mythings.utils.constants;

public class Constants {

	/** SHARED PREFERENCES */
	public final static String PREFERENCES_NAME = "KioliPref";
	public final static String PREF_VIDEO_ELAPSED = "elapsed time";

	/** GOOGLE ANALYTICS */
	// test key
	public final static String GOOGLE_ANALYTICS_ID = "UA-35088722-2";

	// tags
	public final static String ANALYTICS_BASE = "/kioli";
	public final static String ANALYTICS_ACTIVITY = ANALYTICS_BASE + "/analyzed";

	/** CONNECTION TIMERS */
	public final static int CONNECTION_TIMEOUT = 60000;		// 1 minute
	public final static int SOCKET_TIMEOUT = 60000;			// 1 minute

	/** FRAGMENT TAGS*/
	public final static String FR_MAIN = "main";

	/** DATABASE KEYS*/
	public final static String KEY_DEVICEID = "did";
	public final static String KEY_TERMS_AND_CONDITIONS = "t&c";

	/** PARCELABLE INTENT LABLE*/
	public final static String PARCELABLE_INTENT = "parcelable";

	/** MAP ACTIVITY CODE*/
	public final static int GPS_REQUESTCODE = 0;

	/** CONTENT PROVIDER TABLE ID*/
	public final static int TABLE_ENG_CHI_AIRPORTS = 0;
	public final static int TABLE_COUNTRIES = 1;

	/** CONTENT PROVIDER (DB)*/
	public final static int DB_VERSION = 1;
	public final static String EXCEPTION_URI_UNKNOWN = "URI content provider unknown: ";
	public final static String EXCEPTION_URI_NOT_SUPPORTED = "URI content provider not supported: ";
}