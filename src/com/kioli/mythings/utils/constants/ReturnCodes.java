package com.kioli.mythings.utils.constants;

/** Class to store return codes received when connecting to WowAPI */

public final class ReturnCodes {

	/** error code to identify json parsing errors. */
	public static final int NETWORKERROR = 99;

	public static final int RESULTOK = 200;
	public static final int INVALIDINPUT = 400;
	public static final int NOTFOUND = 404;
	public static final int PRIZEALREADYREDEEMED = 420;
	public static final int DEVICEALREADYREGISTERED = 421;
	public static final int CANTPLAY = 422;
	public static final int SERVERERROR = 500;

}