/* MyProject
 *
 * Class BetterActivity (extends Application)
 *
 * Use:
 * This class extends Activity and it's used for shaping some of the pre-existent
 * methods of class Activity basing the new ones on to our needs
 *
 */
package com.kioli.mythings.utils.betterComponents;

import com.kioli.mythings.utils.MyLog;
import android.app.Activity;
import android.app.Application;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;

public abstract class BetterActivity extends Activity {

	// The top level content view.
	private ViewGroup m_contentView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		System.gc();
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		System.gc();
	}

	@Override
	public void setContentView(int layoutResID) {
		setContentView(LayoutInflater.from(this).inflate(layoutResID, null));
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		m_contentView = (ViewGroup) view;
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
		super.setContentView(view, params);
		m_contentView = (ViewGroup) view;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// Fixes android memory issue 8488: http://code.google.com/p/android/issues/detail?id=8488
		nullViewDrawablesRecursive(m_contentView);
		m_contentView = null;
		System.gc();
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		Window window = getWindow();
		window.setFormat(PixelFormat.RGBA_8888);
	}

	private void nullViewDrawablesRecursive(View view) {
		if (view != null) {
			if (view instanceof ViewGroup) {
				try {
					ViewGroup viewGroup = (ViewGroup) view;
					int childCount = viewGroup.getChildCount();
					for (int index = 0; index < childCount; index++) {
						View child = viewGroup.getChildAt(index);
						nullViewDrawablesRecursive(child);
					}
				} catch (Exception e) {
					MyLog.i("BetterActivity.nullDrawableRecursive", "exception getting view children: " + e);
				}
			}
			nullViewDrawable(view);
		}
	}

	private void nullViewDrawable(View view) {
		try {
			view.setBackgroundDrawable(null);
		} catch (Exception e) {
			MyLog.i("BetterActivity.nullDrawable", "exception resetting view background: " + e);
		}
		try {
			ImageView imageView = (ImageView) view;
			imageView.setImageDrawable(null);
			imageView.setBackgroundDrawable(null);
		} catch (Exception e) {
			MyLog.i("BetterActivity.nullDrawable", "exception resetting imageview background: " + e);
		}
	}

	public Application getMyApplication() {
		return (this.getApplication());
	}
}