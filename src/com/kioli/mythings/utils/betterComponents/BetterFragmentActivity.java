package com.kioli.mythings.utils.betterComponents;

import com.kioli.mythings.utils.MyLog;
import android.app.Application;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;

public abstract class BetterFragmentActivity extends FragmentActivity {

	private ViewGroup m_contentView = null;		// The top level content view.

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		System.gc();
	}

	@Override
	protected void onPause() {
		super.onPause();
		System.gc();
	}

	@Override
	public void setContentView(int layoutResID) {
		setContentView(LayoutInflater.from(this).inflate(layoutResID, null));
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		m_contentView = (ViewGroup) view;
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
		super.setContentView(view, params);
		m_contentView = (ViewGroup) view;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		// Fixes android memory issue 8488 : http://code.google.com/p/android/issues/detail?id=8488
		nullViewDrawablesRecursive(m_contentView);
		m_contentView = null;
		System.gc();
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		getWindow().setFormat(PixelFormat.RGBA_8888);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_DITHER, WindowManager.LayoutParams.FLAG_DITHER);
	}

	public Application getMyApplication() {
		return (getApplication());
	}

	private void nullViewDrawablesRecursive(View view) {
		if (view != null) {
			if (view instanceof ViewGroup) {
				try {
					ViewGroup viewGroup = (ViewGroup) view;
					int childCount = viewGroup.getChildCount();
					for (int index = 0; index < childCount; index++) {
						View child = viewGroup.getChildAt(index);
						nullViewDrawablesRecursive(child);
					}
				} catch (Exception e) {
					MyLog.i("BetterFragmentActivity.nullDrawableRecursive", "exception getting view children: " + e);
				}
			}
			nullViewDrawable(view);
		}
	}

	private void nullViewDrawable(View view) {
		try {
			view.setBackgroundDrawable(null);
		} catch (Exception e) {
			MyLog.i("BetterFragmentActivity.nullDrawable", "exception resetting view background: " + e);
		}
		if (view instanceof ImageView) {
			try {
				((ImageView) view).setImageDrawable(null);
			} catch (Exception e) {
				MyLog.i("BetterFragmentActivity.nullDrawable", "exception resetting imageview background: " + e);
			}
		}
	}
}