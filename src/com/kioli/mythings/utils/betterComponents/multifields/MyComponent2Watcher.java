package com.kioli.mythings.utils.betterComponents.multifields;

import android.text.Editable;

import com.kioli.mythings.utils.betterComponents.multifields.generic.AbstractComponent;
import com.kioli.mythings.utils.betterComponents.multifields.generic.AbstractTextWatcher;
import com.kioli.mythings.utils.betterComponents.multifields.generic.IWatcher;

public class MyComponent2Watcher extends AbstractTextWatcher {

	public MyComponent2Watcher(AbstractComponent comp) {
		super(comp);
	}

	private IWatcher watcher_controller;

	public void setWatcherCallback(final IWatcher callback) {
		watcher_controller = callback;
	}

	@Override
	public void checkValidInput(final Editable s) {
		final String text = s.toString();

		if (isTyped) {
			if (text.matches(Controller.patternLetters)) {
				if (text.length() == 0) {
					if (oldText.length() > 0) {
						watcher_controller.onEmpty(getTypeComponent());
					}
				} else {
					watcher_controller.onInputTypedOk(getTypeComponent(), text);
				}
			} else {
				revertTo(s, oldText);
				watcher_controller.onInputTypedWrong(text);
			}
		} else {
			if (text.matches(Controller.patternLetters)) {
				watcher_controller.onInputPastedOk(text);
			} else {
				revertTo(s, oldText);
				watcher_controller.onInputPastedWrong(text);
			}
		}
	}
}