package com.kioli.mythings.utils.betterComponents.multifields;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kioli.mythings.utils.betterComponents.multifields.generic.AbstractComponent;
import com.kioli.mythings.utils.betterComponents.multifields.generic.IEditText;
import com.kioli.mythings.utils.betterComponents.multifields.generic.IWatcher;
import com.kioli.mythings.utils.betterComponents.multifields.generic.MyKeyListener;

public class Controller extends LinearLayout implements IWatcher, IEditText {

	public enum COMPONENT {
		WTC_MYCMP1, WTC_MYCMP2;
	}

	private ArrayList<AbstractComponent> components;
	private final String TAG_LOG = "Components Controller";

	// controls for component 1
	public final static String patternNumbers = "^[0-9]{0,10}$";
	public final static int MAX_NUMERS_LENGTH = 10;

	// controls for component 2
	public final static String patternLetters = "^[a-zA-Z]{0,5}$";
	public final static int MAX_LETTERS_LENGTH = 5;

	public Controller(final Context context) {
		super(context);
	}

	public Controller(final Context context, final AttributeSet attrs) {
		super(context, attrs);

		initComponents(context, attrs);
	}

	/** Set the list of components held by this class */
	private void initComponents(final Context context, final AttributeSet attrs) {
		components = new ArrayList<AbstractComponent>();

		final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);

		// Add component 1
		final AbstractComponent comp = new MyComponent1(context, attrs);

		final MyComponent1Watcher watch = new MyComponent1Watcher(comp);
		watch.setWatcherCallback(this);
		
		final MyKeyListener key_listener = new MyKeyListener(comp);
		key_listener.setCallbackComponent(this);
		
		comp.setWatcher(watch);
		comp.setOnKeyListener(key_listener);
		comp.setNextFocusLength(MAX_NUMERS_LENGTH);
		comp.setLayoutParams(params);

		components.add(comp);
		addView(comp);
		
		// Add component 2
		final AbstractComponent comp2 = new MyComponent2(context, attrs);

		final MyComponent2Watcher watch2 = new MyComponent2Watcher(comp2);
		watch2.setWatcherCallback(this);

		final MyKeyListener key_listener2 = new MyKeyListener(comp2);
		key_listener2.setCallbackComponent(this);
		
		comp2.setWatcher(watch2);
		comp2.setOnKeyListener(key_listener2);
		comp2.setNextFocusLength(MAX_LETTERS_LENGTH);
		comp2.setLayoutParams(params);

		components.add(comp2);
		addView(comp2);
	}
	
	private void moveFocusAhead(final AbstractComponent comp) {
		int index = components.indexOf(comp);

		if (index < components.size() - 1) {
			AbstractComponent component = components.get(index + 1);
			component.requestFocus();
		}
	}
	
	private void moveFocusBack(final AbstractComponent comp) {
		int index = components.indexOf(comp);

		if (index > 0) {
			AbstractComponent component = components.get(index - 1);
			component.requestFocus();
		}
	}

	@Override
	public void onEmpty(final AbstractComponent comp) {
		Log.e(TAG_LOG, "OnEmpty triggered");
		comp.setIsEmpty(true);
	}

	@Override
	public void onInputTypedWrong(final String text) {
		Log.e(TAG_LOG, "onInputTypedWrong triggered");
	}

	@Override
	public void onInputTypedOk(final AbstractComponent comp, final String text) {
		Log.e(TAG_LOG, "onInputTypedOk triggered");
		comp.setIsEmpty(false);
		
		if(text.length() >= comp.getNextFocusLength()) {
			moveFocusAhead(comp);
		}
	}

	@Override
	public void onInputPastedWrong(final String text) {
		Toast.makeText(getContext(),"onInputPastedWrong triggered", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onInputPastedOk(final String text) {
		Log.e(TAG_LOG, "onInputPastedOk triggered");
	}

	@Override
	public void onDeletePressed(AbstractComponent comp) {
		moveFocusBack(comp);
	}

	@Override
	public void onNextPressed(AbstractComponent comp) {
		moveFocusAhead(comp);
	}
	
	public Bundle saveInstanceState(Bundle outState) {
		for (int i = 0; i < components.size(); i++) {
			outState.putString("component" + i, components.get(i).getData());
		}
		return outState;
	}

	public void restoreInstanceState(Bundle savedInstanceState) {
		for (int i = 0; i < components.size(); i++) {
			components.get(i).setData(savedInstanceState.getString("component" + i));
		}
	}
}
