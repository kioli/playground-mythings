package com.kioli.mythings.utils.betterComponents.multifields;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.multifields.generic.AbstractComponent;
import com.kioli.mythings.utils.betterComponents.multifields.generic.AbstractTextWatcher;

public class MyComponent1 extends AbstractComponent {

	private final EditText text;
	private AbstractTextWatcher watcher;

	public MyComponent1(final Context context, final AttributeSet attrs) {
		super(context, attrs);

		final View layout = LayoutInflater.from(context).inflate(
				R.layout.my_component1, MyComponent1.this);

		text = (EditText) layout.findViewById(R.id.text_field1);

		text.setImeOptions(EditorInfo.IME_ACTION_NEXT);
	}

	@Override
	public void setWatcher(AbstractTextWatcher watch) {
		watcher = watch;
		text.addTextChangedListener(watcher);
	}

	@Override
	public void clear() {
		text.setText("");
	}

	@Override
	public String getData() {
		return text.getText().toString();
	}

	@Override
	public void setData(final String data) {
		text.setText(data);
	}

	@Override
	public void formatField(final boolean hasFocus) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean handleNextKey() {
		// TODO Auto-generated method stub
		return false;
	}
}
