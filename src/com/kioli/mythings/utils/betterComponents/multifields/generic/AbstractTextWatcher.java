package com.kioli.mythings.utils.betterComponents.multifields.generic;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public abstract class AbstractTextWatcher implements TextWatcher {

	protected boolean isTyped;
	protected int startPastingIndex;
	protected int lengthPastedText;
	protected EditText curInputFocus;
	protected String oldText;
	protected int cursorPosition;
	protected boolean preventLoop;

	private AbstractComponent component;
	
	public AbstractTextWatcher(AbstractComponent comp) {
		component = comp;
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start,
			final int count, final int after) {
		if (preventLoop) {
			return;
		}

		oldText = new String(s.toString());

		cursorPosition = start + after;
		isTyped = ((after - count > 1) ? false : true);

		if (isTyped) {
			startPastingIndex = 0;
			lengthPastedText = 0;
		} else {
			startPastingIndex = start;
			lengthPastedText = after;
		}
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start,
			final int before, final int count) {
		// nothing
	};

	@Override
	public void afterTextChanged(final Editable s) {
		if (preventLoop) {
			return;
		}
		preventLoop = true;
		checkInput(s);
		preventLoop = false;
	}

	private void checkInput(final Editable text) {
		if (isTyped == false) {
			getPastedText(text);
		}

		checkValidInput(text);
	}

	public void getPastedText(final Editable s) {
		// Set the input to get rid of what was written there before
		final String totalInput = s.toString();
		final String cleanedInput = totalInput.subSequence(startPastingIndex,
				startPastingIndex + lengthPastedText).toString();
		revertTo(s, cleanedInput);
	}

	/**
	 * Method that recover the string as it was before the current modification
	 * 
	 * @param s
	 *            Editable currently on modification
	 * @param before
	 *            String previous content
	 */
	public void revertTo(final Editable s, final String before) {
		if (cursorPosition > 0) {
			cursorPosition--;
		}
		s.clear();
		s.append(before);
	}

	public void clearOldText() {
		if (oldText != null) {
			oldText = "";
		}
	}

    public AbstractComponent getTypeComponent() {
    	return component;
    }
    
    public void setTypeComponent(AbstractComponent type) {
    	component = type;
    }
    
	public abstract void checkValidInput(final Editable text);
}
