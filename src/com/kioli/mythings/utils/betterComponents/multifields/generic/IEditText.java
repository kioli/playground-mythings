package com.kioli.mythings.utils.betterComponents.multifields.generic;

public interface IEditText {
	public void onDeletePressed(AbstractComponent component);

	public void onNextPressed(AbstractComponent component);
}
