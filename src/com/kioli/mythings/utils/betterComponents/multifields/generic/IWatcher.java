package com.kioli.mythings.utils.betterComponents.multifields.generic;

public interface IWatcher {
	public void onEmpty(AbstractComponent comp);

	public void onInputTypedWrong(String text);

	public void onInputTypedOk(AbstractComponent comp, String text);

	public void onInputPastedWrong(String text);

	public void onInputPastedOk(String text);
}