package com.kioli.mythings.utils.betterComponents.multifields.generic;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public abstract class AbstractComponent extends LinearLayout {
	
	private int FOCUS_NEXT_LENGTH;
	private boolean isEmpty;
    
    public AbstractComponent(final Context context) {
        super(context);
    }

    public AbstractComponent(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }
    
    public boolean checkEmptyData() {
		if (getData().length() > 0) {
            return false;
        }
        return true;
    }
    
    public int getNextFocusLength() {
    	return FOCUS_NEXT_LENGTH;
    }
    
    public void setNextFocusLength(int length) {
    	FOCUS_NEXT_LENGTH = length;	
    }
    
    public boolean getIsEmpty() {
    	return isEmpty;
    }
    
    public void setIsEmpty(final boolean isEmpty) {
    	this.isEmpty = isEmpty;
    }
    
    public abstract void clear();

	public abstract String getData();

	public abstract void setData(final String data);

    public abstract void formatField(boolean hasFocus);

	public abstract boolean handleNextKey();

	public abstract void setWatcher(AbstractTextWatcher watch);
}