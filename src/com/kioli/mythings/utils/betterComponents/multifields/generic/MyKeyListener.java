package com.kioli.mythings.utils.betterComponents.multifields.generic;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;

public class MyKeyListener implements OnKeyListener {

	public IEditText callback_component;

	private AbstractComponent component;

	public MyKeyListener(AbstractComponent comp) {
		component = comp;
	}

	public void setCallbackComponent(IEditText callback) {
		callback_component = callback;
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (event.getKeyCode()) {
			case KeyEvent.KEYCODE_DEL:
				callback_component.onDeletePressed(component);
				return true;
			case KeyEvent.KEYCODE_ENTER:
				callback_component.onNextPressed(component);
				return true;
			default:
				break;
			}
		}
		return false;
	}

}
