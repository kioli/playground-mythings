package com.kioli.mythings.utils.betterComponents.multifields;

import android.app.Activity;
import android.os.Bundle;

import com.google.analytics.tracking.android.Log;
import com.kioli.mythings.R;

public class ActivityMultifields extends Activity {

	Controller component;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multifield);

		component = (Controller) findViewById(R.id.multifield);

		// Recover previous state if activity was abruptly destroyed
		if (savedInstanceState != null) {
			component.restoreInstanceState(savedInstanceState);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		component.saveInstanceState(outState);
	}
}
