package com.kioli.mythings.utils.betterComponents.multifields;

import android.text.Editable;

import com.kioli.mythings.utils.betterComponents.multifields.generic.AbstractComponent;
import com.kioli.mythings.utils.betterComponents.multifields.generic.AbstractTextWatcher;
import com.kioli.mythings.utils.betterComponents.multifields.generic.IWatcher;

public class MyComponent1Watcher extends AbstractTextWatcher {

	public MyComponent1Watcher(AbstractComponent comp) {
		super(comp);
	}

	private IWatcher watch_controller;	

	public void setWatcherCallback(final IWatcher callback) {
		watch_controller = callback;
	}

	@Override
	public void checkValidInput(final Editable s) {
		final String text = s.toString();

		if (isTyped) {
			if (text.matches(Controller.patternNumbers)) {
				if (text.length() == 0) {
					if (oldText.length() > 0) {
						watch_controller.onEmpty(getTypeComponent());
					}
				} else {
					watch_controller.onInputTypedOk(getTypeComponent(), text);
				}
			} else {
				revertTo(s, oldText);
				watch_controller.onInputTypedWrong(text);
			}
		} else {
			if (text.matches(Controller.patternNumbers)) {
				watch_controller.onInputPastedOk(text);
			} else {
				revertTo(s, oldText);
				watch_controller.onInputPastedWrong(text);
			}
		}
	}
}