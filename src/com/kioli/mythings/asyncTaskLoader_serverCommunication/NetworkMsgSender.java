package com.kioli.mythings.asyncTaskLoader_serverCommunication;

import java.io.ByteArrayOutputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;
import android.content.Context;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.constants.Constants;

/** @author Class for sending and reading data to/from the server */
public class NetworkMsgSender {

	/** Constant for debug and log */
	private static final String CLASSTAG = "NetworkMsgSender";

	private final DefaultHttpClient httpclient;
	private static int numberExternalHost;
	private static long time;

	// private final Context context;

	public NetworkMsgSender(Context ctx) {
		// context = ctx;

		// Keeping the http client makes easier to manage cookies and related stuff.
		HttpParams httpParameters = new BasicHttpParams();
		NetworkMsgSender.numberExternalHost = 0;
		NetworkMsgSender.time = System.currentTimeMillis();

		// Set the timeout for the connection to be established.
		HttpConnectionParams.setConnectionTimeout(httpParameters, Constants.CONNECTION_TIMEOUT);

		// Set the default socket timeout which is the timeout for waiting for data.
		HttpConnectionParams.setSoTimeout(httpParameters, Constants.SOCKET_TIMEOUT);
		httpclient = new DefaultHttpClient(httpParameters);
	}

	/** Method that call to a script using POST and specifying its parameters.
	 *
	 * @param url
	 *        Script URL.
	 * @param params
	 *        Post data sent.
	 * @return Response from server if it is expected and everything is OK, null otherwise
	 * @throws DataNotChangedException */
	public JSONObject postRequest(NetworkRequest nr) {
		JSONObject result = new JSONObject();

		if (NetworkMsgSender.numberExternalHost < 2 || (System.currentTimeMillis() - NetworkMsgSender.time > 30000)) {
			MyLog.w(NetworkMsgSender.CLASSTAG, "New request(" + Thread.currentThread().getName() + "): " + nr.getURL());
			try {
				// set the URL to which send the http request
				final HttpPost httpost = new HttpPost(nr.getURL());
				HashMap<String, String> params = nr.getParameters();
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				if (params != null) {
					for (Map.Entry<String, String> entry : params.entrySet()) {
						MyLog.i("NetwReq", entry.getKey() + entry.getValue());
						nameValuePairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
					}
					httpost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF8"));
				}

				// execute the http request and get the result
				HttpResponse response = this.httpclient.execute(httpost);
				final ByteArrayOutputStream b = new ByteArrayOutputStream();
				response.getEntity().writeTo(b);
				final String s = new String(b.toByteArray());
				result = new JSONObject(s);
				MyLog.i("NetwReq", "RESULT: " + result);
			} catch (UnknownHostException e) {
				NetworkMsgSender.numberExternalHost++;
				NetworkMsgSender.time = System.currentTimeMillis();
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}
		return result;
	}

	// public void sendStats(NetworkRequest nr) {
	// try {
	// // set the URL to which send the http request
	// final HttpPost httpost = new HttpPost(nr.getURL());
	// String appName = context.getString(R.string.app_name);
	// String version = context.getString(R.string.app_version_number);
	// String model = android.os.Build.MODEL;
	// String manufacturer = android.os.Build.BRAND;
	// String release = android.os.Build.VERSION.RELEASE;
	// httpost.setHeader("User-Agent", appName + "+" + version + "(Android;+Android+OS+" + release +
	// ";Android+Manufacturer+" + manufacturer
	// + ";+Android+Model+" + model + ")");
	//
	// // execute the http request and get the result
	// HttpResponse response = httpclient.execute(httpost);
	// final ByteArrayOutputStream b = new ByteArrayOutputStream();
	// response.getEntity().writeTo(b);
	// final String s = new String(b.toByteArray());
	// MyLog.i("Stats", s);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
}