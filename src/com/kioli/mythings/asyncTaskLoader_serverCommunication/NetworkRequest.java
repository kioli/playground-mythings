package com.kioli.mythings.asyncTaskLoader_serverCommunication;

import java.util.HashMap;
import android.os.Handler;

/** Class used to pass requests to the Network service, and bind a handler to populate the new data. */
public class NetworkRequest {

	String URL;
	HashMap<String, String> parameters;
	Handler responseHandler;

	/** @param URL
	 *        URL for the request.
	 * @param parameters
	 *        HashMap containing post parameters for the request */
	public NetworkRequest(String URL, HashMap<String, String> parameters) {
		this.URL = URL;
		this.parameters = parameters;
	}

	public String getURL() {
		return this.URL;
	}

	public HashMap<String, String> getParameters() {
		return this.parameters;
	}
}