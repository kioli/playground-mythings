package com.kioli.mythings.asyncTaskLoader_serverCommunication;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import com.kioli.mythings.utils.MyLog;

/** AsyncTaskLoader used to obtain some data from a webservice */
public class ServerDataLoader extends AsyncTaskLoader<JSONObject> {

	private JSONObject jsonResult;
	private final Context context;
	private HashMap<String, String> params;

	private final String URL = "http://ws.geonames.org/earthquakesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2";
	private final String result_tag = "earthquakes";

	private ServerDataLoader(Context ctx) {
		super(ctx);
		context = ctx;
	}

	public static ServerDataLoader newInstance(Context ctx) {
		ServerDataLoader loader = new ServerDataLoader(ctx);
		return loader;
	}

	/** Set arguments to be passed to the webservice. Arguments are passed in a Bundle (since this is what
	 * LoaderCallBacks.onCreateLoader gets)
	 *
	 * @param args
	 *        Bundle containing arguments to pass to the webservice */
	public void setParams(Bundle args) {
		try {
			params = new HashMap<String, String>();
			Set<String> keys = args.keySet();
			for (String key : keys) {
				params.put(key, args.getString(key));
			}
		} catch (Exception e) {
			MyLog.e("ServerDataLoader", "Exception setting parameters: " + e);
		}
	}

	@Override
	public JSONObject loadInBackground() {
		JSONObject jsonToReturn;

		// If you receive a JSON
		jsonToReturn = loadJson();
		// If you receive a JSON of images to download and store locally
		// jsonToReturn = loadImages();

		return jsonToReturn;
	}

	private JSONObject loadJson() {
		jsonResult = new JSONObject();
		final NetworkRequest nr = new NetworkRequest(URL, params);

		// Result coming from the server at the given URL
		JSONObject result = new NetworkMsgSender(context).postRequest(nr);
		try {
			JSONArray array = result.getJSONArray(result_tag);
			jsonResult = array.getJSONObject(0);
		} catch (Exception e) {
			MyLog.e("ServerDataLoader.loadInBackground", "Exception downloading JSON: " + e);
		}
		return jsonResult;
	}

	private JSONObject loadImages() {
		for (int i = 0; i < params.size(); i++) {
			try {
				URL url = new URL(params.get(i));
				URLConnection conection = url.openConnection();
				conection.connect();

				InputStream input = new BufferedInputStream(url.openStream(), 8192);
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				int size = 0;

				byte data[] = new byte[1024];
				while ((size = input.read(data, 0, 1024)) >= 0) {
					outputStream.write(data, 0, size);
				}
				input.close();

				data = outputStream.toByteArray();

				FileOutputStream fos;

				fos = context.openFileOutput("image" + i + ".png", Context.MODE_PRIVATE);
				fos.write(data);
				fos.close();

			} catch (Exception e) {
				MyLog.e("ServerDataLoader.loadInBackground", "Exception downloading images: " + e);
			}
		}
		return null;
	}

	/** Handles a request to start the Loader. */
	@Override
	protected void onStartLoading() {
		if (jsonResult != null) {
			deliverResult(jsonResult);
		} else {
			forceLoad();
		}
	}

	/** Handles a request to stop the Loader. */
	@Override
	protected void onStopLoading() {
		cancelLoad();
	}

	/** Handles a request to completely reset the Loader. */
	@Override
	protected void onReset() {
		super.onReset();
		onStopLoading();
		jsonResult = null;
	}
}