package com.kioli.mythings.asyncTaskLoader_serverCommunication;

import org.json.JSONObject;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.betterComponents.BetterFragmentActivity;

public class ServerCommunicationActivity extends BetterFragmentActivity {

	private static String KEY_INPUT1 = "input1";
	private static String VALUE_INPUT1 = "fake";
	private static final int LOADER_SERVER_DATA = 0;

	private static TextView txt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_server_communication);
		init();
	}

	private void init() {
		txt = (TextView) findViewById(R.id.txt_server_comm);
		findViewById(R.id.btn_server_communication).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				launchLoader();
			}
		});
	}

	private void launchLoader() {
		// Set the data the webservice needs to receive
		Bundle args = new Bundle();
		args.putString(KEY_INPUT1, VALUE_INPUT1);

		if (getSupportLoaderManager().getLoader(LOADER_SERVER_DATA) == null) {
			getSupportLoaderManager().initLoader(LOADER_SERVER_DATA, args, server_data_loader);
		} else {
			getSupportLoaderManager().restartLoader(LOADER_SERVER_DATA, args, server_data_loader);
		}
	}

	LoaderCallbacks<JSONObject> server_data_loader = new LoaderCallbacks<JSONObject>() {

		@Override
		public Loader<JSONObject> onCreateLoader(int id, Bundle args) {
			ServerDataLoader loader = ServerDataLoader.newInstance(ServerCommunicationActivity.this);
			loader.setParams(args);
			return loader;
		}

		@Override
		public void onLoadFinished(Loader<JSONObject> arg0, JSONObject data) {
			MyLog.v("LoaderCallBack", "ServerData received");
			if (data.length() != 0) {
				txt.setText(data.toString());
			} else {
				showErrorConnectionDialog();
			}
		}

		@Override
		public void onLoaderReset(Loader<JSONObject> arg0) {
		}
	};

	private void showErrorConnectionDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getResources().getString(R.string.error_dialog_network_txt))
				.setTitle(getResources().getString(R.string.error_dialog_title)).setCancelable(false)
				.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
}