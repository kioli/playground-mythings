package com.kioli.mythings.db.utils;

final public class EnumUtil {
	
	/** Hide constructor for utility classes. */
    private EnumUtil() { }

    public static <E extends Enum<E>> int toIntValue(final E value) {
        if (value == null) {
            return -1;
        } else {
            return value.ordinal();
        }
    }

    public static <E extends Enum<E>> E fromIntValue(final Class<E> cls, final int ord) {
        if (ord == -1) {
            return null;
        } else {
            final E[] values = cls.getEnumConstants();
            return values[ord];
        }
    }
}
