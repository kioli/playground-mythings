package com.kioli.mythings.db;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.kioli.mythings.R;

public class DetailsFragment extends Fragment {
	private WebView	viewer;

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		viewer = (WebView) inflater.inflate(R.layout.activity_db_item_view, container, false);
		return viewer;
	}

	public void updateUrl(final String newUrl) {
		if (viewer != null) {
			viewer.loadUrl(newUrl);
		}
	}
}