package com.kioli.mythings.db;

import static com.kioli.mythings.db.db.DbVersion.CURRENT_VERSION;
import static com.kioli.mythings.db.db.DbVersion.DATABASE_VERSION_1;
import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.kioli.mythings.R;
import com.kioli.mythings.db.db.DbProvider;
import com.kioli.mythings.db.db.models.DatabaseDao1;
import com.kioli.mythings.db.db.models.DatabaseDao2;

public class MainListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
	public interface OnItemSelectedListener {
		public void onSelected(String tutUrl);
	}

	private OnItemSelectedListener	tutSelectedListener;

	private static final int	   TUTORIAL_LIST_LOADER	= 0x01;
	private SimpleCursorAdapter	   adapter;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final String[] uiBindFrom = { DatabaseDao1.COL_TITLE };
		final int[] uiBindTo = { R.id.title };
		getLoaderManager().initLoader(TUTORIAL_LIST_LOADER, null, this);
		adapter = new SimpleCursorAdapter(getActivity().getApplicationContext(), R.layout.activity_db_list_item, null, uiBindFrom,
		        uiBindTo,
		        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		setListAdapter(adapter);
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			tutSelectedListener = (OnItemSelectedListener) activity;
		} catch (final ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnTutSelectedListener");
		}
	}

	@Override
	public void onListItemClick(final ListView l, final View v, final int position, final long id) {
		//XXX: Method made so to behave differently when the device has DB_Version1 or DB_Version2
		String projection[];
		if (CURRENT_VERSION == DATABASE_VERSION_1) {
			projection = new String[] { DatabaseDao1.COL_URL };
		} else {
			projection = new String[] { DatabaseDao2.COL_URL, DatabaseDao2.COL_NEW };
		}

		final Cursor tutorialCursor = getActivity().getContentResolver().query(
		        Uri.withAppendedPath(DbProvider.CONTENT_URI, String.valueOf(id)), projection, null, null, null);
		if (tutorialCursor.moveToFirst()) {
			final String tutorialUrl = tutorialCursor.getString(0);

			if (tutorialCursor.getColumnCount() > 1) {
				Toast.makeText(getActivity(), "NEW DATA", Toast.LENGTH_SHORT).show();
			} else {
				tutSelectedListener.onSelected(tutorialUrl);
			}
		}
		tutorialCursor.close();
	}

	@Override
	public Loader<Cursor> onCreateLoader(final int arg0, final Bundle arg1) {
		final String[] projection = { DatabaseDao1.COL_COUNTER, DatabaseDao1.COL_TITLE };
		final CursorLoader cursorLoader = new CursorLoader(getActivity(), DbProvider.CONTENT_URI, projection, null, null, null);
		return cursorLoader;
	}

	@Override
	public void onLoadFinished(final Loader<Cursor> loader, final Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(final Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}
}