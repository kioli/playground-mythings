package com.kioli.mythings.db;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.kioli.mythings.R;

public class DetailsActivity extends FragmentActivity {
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_db_item_fragment);

		final String content = getIntent().getData().toString();

		((DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.tutview_fragment)).updateUrl(content);
	}
}