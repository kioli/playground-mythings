package com.kioli.mythings.db;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.kioli.mythings.R;

public class MainListActivity extends FragmentActivity implements MainListFragment.OnItemSelectedListener {
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_db_list_fragment);
	}

	@Override
	public void onSelected(final String url) {
		final DetailsFragment details = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.tutview_fragment);

		if (details == null || !details.isInLayout()) {
			final Intent showContent = new Intent(getApplicationContext(), DetailsActivity.class);
			showContent.setData(Uri.parse(url));
			startActivity(showContent);
		} else {
			details.updateUrl(url);
		}
	}
}