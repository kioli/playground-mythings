package com.kioli.mythings.db.db.models;

public class DataModel {

	public long	   counter;
	public String	title;
	public String	url;
	public boolean	newData;

	public DataModel() {
	}

	/** Creates a copy of an existing profile. */
	public DataModel(final DataModel from) {
		this.counter = from.counter;
		this.title = from.title;
		this.url = from.url;
		this.newData = from.newData;
	}
}