package com.kioli.mythings.db.db.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kioli.mythings.db.utils.DbUtils;

public class DatabaseDao1 implements IDatabaseDao {

	// Table structure
	public static final String	COL_TITLE	= "title";
	public static final String	COL_URL	  = "url";

	private static String	   SCHEMA	  = COL_COUNTER + " INTEGER, " + COL_TITLE + " text not null, " + COL_URL + " text not null, "
	                                              + "UNIQUE(" + COL_COUNTER + ")";

	@Override
	public void ensureWritableTable(final SQLiteDatabase db, final String tableName, final boolean clean) {
		DbUtils.ensureWritableTable(db, tableName, SCHEMA, clean);
	}

	@Override
	public void ensureIndexes(final SQLiteDatabase db, final String tableName) {
		//		do nothing
	}

	@Override
	public DataModel read(final Cursor cursor) {
		final DataModel profile = new DataModel();

		profile.counter = cursor.getLong(0);
		profile.title = cursor.getString(1);
		profile.url = cursor.getString(2);

		return profile;
	}

	@Override
	public void write(final DataModel profile, final ContentValues args) {
		args.put(COL_COUNTER, Long.valueOf(profile.counter));
		args.put(COL_TITLE, profile.title);
		args.put(COL_URL, profile.url);
	}
}
