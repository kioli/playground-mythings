package com.kioli.mythings.db.db.models;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public interface IDatabaseDao {

	public static final String	TABLE_NAME	= "table1";
	public static final String	COL_COUNTER	= "_id";

    public void ensureWritableTable(SQLiteDatabase db, String tableName, boolean b);
    public void ensureIndexes(SQLiteDatabase db, String tableName);

    public DataModel read(Cursor cursor);
    public void write(DataModel mutation, ContentValues values);
}
