package com.kioli.mythings.db.db;

import static com.kioli.mythings.db.db.models.IDatabaseDao.TABLE_NAME;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.kioli.mythings.db.db.models.DatabaseDao1;

/**
 * The public definitions will be used by the other portions of the app (or
 * other apps who want to access the content provider data). The private
 * definitions are for internal use by the class only and not exposed to others
 */
public class DbProvider extends ContentProvider {

	private DbHelper	        dbHelper;

	// To put on private when extracting this technique on a real project
	public static final String	AUTHORITY	      = "com.kioli.mythings.db.db.DbProvider";
	public static final int	   CODE_TITLE	= 0;
	public static final int	   CODE_ID	   = 1;
	public static final Uri	   CONTENT_URI	      = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);

	//	public static final String	CONTENT_ITEM_TYPE	= ContentResolver.CURSOR_ITEM_BASE_TYPE + "/tutorial";
	//	public static final String	CONTENT_TYPE	  = ContentResolver.CURSOR_DIR_BASE_TYPE + "/tutorial";

	@Override
	public boolean onCreate() {
		dbHelper = new DbHelper(getContext());
		return true;
	}

	private static final UriMatcher	sURIMatcher	= new UriMatcher(UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, TABLE_NAME, CODE_TITLE);
		sURIMatcher.addURI(AUTHORITY, TABLE_NAME + "/#", CODE_ID);
	}

	@Override
	public int delete(final Uri uri, final String selection, final String[] selectionArgs) {
		final int uriType = sURIMatcher.match(uri);
		final SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
			case CODE_TITLE:
				rowsAffected = sqlDB.delete(DatabaseDao1.TABLE_NAME, selection, selectionArgs);
				break;
			case CODE_ID:
				final String id = uri.getLastPathSegment();
				if (TextUtils.isEmpty(selection)) {
					rowsAffected = sqlDB.delete(DatabaseDao1.TABLE_NAME, DatabaseDao1.COL_COUNTER + "=" + id, null);
				} else {
					rowsAffected = sqlDB.delete(DatabaseDao1.TABLE_NAME, selection + " and " + DatabaseDao1.COL_COUNTER + "=" + id,
					        selectionArgs);
				}
				break;
			default:
				throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(final Uri uri) {
		return null;
	}

	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		if (values != null) {
			SQLiteDatabase db = null;
			try {
				db = this.dbHelper.getWritableDatabase();
			} catch (final Exception e) {
				e.printStackTrace();
			}

			if (db != null) {
				db.beginTransaction();
				try {
					db.insert(uri.getLastPathSegment(), null, values);
					db.setTransactionSuccessful();
				} catch (final Exception e) {
					e.printStackTrace();
				} catch (final OutOfMemoryError e) {
					e.printStackTrace();
				} finally {
					db.endTransaction();
				}
			}
		}
		return uri;
	}

	@Override
	public Cursor query(final Uri uri, final String[] projection, final String selection, final String[] selectionArgs,
	        final String sortOrder) {
		final SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(DatabaseDao1.TABLE_NAME);
		final int uriType = sURIMatcher.match(uri);
		switch (uriType) {
			case CODE_ID:
				queryBuilder.appendWhere(DatabaseDao1.COL_COUNTER + "=" + uri.getLastPathSegment());
				break;
			case CODE_TITLE:
				// no filter
				break;
			default:
				throw new IllegalArgumentException("Unknown URI");
		}
		final Cursor cursor = queryBuilder.query(dbHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null,
		        sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(final Uri uri, final ContentValues values, final String selection, final String[] selectionArgs) {
		return 0;
	}

	@Override
	public int bulkInsert(final Uri uri, final ContentValues[] values) {
		if (values != null) {
			SQLiteDatabase db = null;
			try {
				db = dbHelper.getWritableDatabase();
			} catch (final Exception e) {
				e.printStackTrace();
			}

			if (db != null) {
				db.beginTransaction();
				try {
					for (final ContentValues cv : values) {
						db.replace(uri.getLastPathSegment(), null, cv);
					}

					db.setTransactionSuccessful();
				} catch (final Exception e) {
					e.printStackTrace();
				} catch (final OutOfMemoryError e) {
					e.printStackTrace();
				} finally {
					db.endTransaction();
				}
			}
		}
		return values.length;
	}
}