package com.kioli.mythings.db.db;

import com.kioli.mythings.db.db.models.DatabaseDao1;
import com.kioli.mythings.db.db.models.DatabaseDao2;
import com.kioli.mythings.db.db.models.IDatabaseDao;

/**
 * When creating a database for the first time your {@link DbHelper} provides CURRENT_VERSION as a version.
 * That value gets recorded in the database file. The hard-coded value of CURRENT_VERSION stays in the APK.
 *
 * If a change to the DB schema is made for the new release you increment the CURRENT_VERSION in your code. When the
 * new release is installed, it will open the database. But now it will find that the value hard-coded in the APK is
 * different from the value recorded in DB file. This will trigger the upgrade scenario. After the upgrade completes
 * the new CURRENT_VERSION value is saved to the DB file.<br><br>
 *
 * So, important things are:<br><br>
 *    1) When changing the schema add a new DATABASE_VERSION_X constant. Assign it to the CURRENT_VERSION.<br>
 *    2) Once added a DATABASE_VERSION_X constant NEVER CHANGE IT EVERAFTER.<br>
 *    3) Implement new clause for your DATABASE_VERSION_X in the ProfileDbHelper#onUpdate method.<br>
 */

final public class DbVersion {
	public static final int	DATABASE_VERSION_1	= 1;
	public static final int	DATABASE_VERSION_2	= 2;
    /* Add a new DATABASE_VERSION_X here, but never ever change those above. */

	public static final int	CURRENT_VERSION	   = DATABASE_VERSION_1;


	public static IDatabaseDao daoForVersion(final int version) {
        switch (version) {
			case DATABASE_VERSION_1:
				return new DatabaseDao1();
			case DATABASE_VERSION_2:
				return new DatabaseDao2();
            /* Add DAO for a new version here. */

			default:
				throw new UnsupportedOperationException("No Profile DAO class provided for database version " + version);
        }
    }

    private DbVersion() { }
}
