package com.kioli.mythings.db.db;

import static com.kioli.mythings.db.db.DbVersion.CURRENT_VERSION;
import static com.kioli.mythings.db.db.DbVersion.DATABASE_VERSION_1;
import static com.kioli.mythings.db.db.DbVersion.DATABASE_VERSION_2;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kioli.mythings.db.db.models.DatabaseDao1;
import com.kioli.mythings.db.db.models.DatabaseDao2;
import com.kioli.mythings.db.db.models.IDatabaseDao;

public class DbHelper extends SQLiteOpenHelper {

	public static final String	DEBUG_TAG	= "DB TEST";

	private static final String	DB_NAME	  = "list_database";
	private static DbHelper	    helper;
	private final Context	    context;
	private IDatabaseDao	    daoProfile;
	private boolean	            isDaoCreated;

	public static DbHelper getHelper(final Context context) {
		if (helper == null) {
			helper = new DbHelper(context.getApplicationContext());
		}
		return helper;
	}

	public DbHelper(final Context context) {
		// WARNING: call this constructor only with application context to avoid leaks
		super(context, DB_NAME, null, CURRENT_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		UpgradeHelper.createTable(db, CURRENT_VERSION);
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
		switch (oldVersion) {
			case DATABASE_VERSION_1:
				UpgradeHelper.upgradeTableFrom(db, oldVersion, newVersion);
				break;
			default:
				UpgradeHelper.dropTable(db);
				onCreate(db);
				break;
		}
	}

	public static IDatabaseDao daoForVersion(final int version) {
		switch (version) {
			case DATABASE_VERSION_1:
				return new DatabaseDao1();
			case DATABASE_VERSION_2:
				return new DatabaseDao2();
				/* Add DAO for a new version here. */

			default:
				throw new UnsupportedOperationException("No Profile DAO class provided for database version " + version);
		}
	}

	public static String makeDropSql(final String tableName) {
		return "DROP TABLE IF EXISTS " + tableName;
	}

	/**
	 * Assure that all DAOs get the same key and cipher when instantiated.
	 */
	private void ensureDaoObjects() {
		if (!isDaoCreated) {
			daoProfile = daoForVersion(CURRENT_VERSION);
			/* Add creating more DAO objects here */

			isDaoCreated = true;
		}
	}

	public IDatabaseDao getProfileDao() {
		ensureDaoObjects();
		return daoProfile;
	}
}