package com.kioli.mythings.db.db;

import static com.kioli.mythings.db.db.DbVersion.DATABASE_VERSION_1;
import static com.kioli.mythings.db.db.DbVersion.DATABASE_VERSION_2;
import static com.kioli.mythings.db.db.DbVersion.daoForVersion;
import static com.kioli.mythings.db.db.models.IDatabaseDao.TABLE_NAME;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kioli.mythings.db.db.models.DataModel;
import com.kioli.mythings.db.db.models.IDatabaseDao;
import com.kioli.mythings.db.utils.DbUtils;

public class UpgradeHelper {

	private static final String	SQL_GET_ALL	= "select * from " + TABLE_NAME;

	public static void dropTable(final SQLiteDatabase db) {
		db.execSQL(DbUtils.makeDropSql(TABLE_NAME));
	}

	public static void createTable(final SQLiteDatabase db, final int version) {
		final IDatabaseDao newDao = daoForVersion(version);
		newDao.ensureWritableTable(db, TABLE_NAME, true);
		newDao.ensureIndexes(db, TABLE_NAME);

		// Fill DB initially... To be defined how otherwise
		final ContentValues values = new ContentValues(2);
		final DataModel data = new DataModel();
		data.title = "Paul";
		data.url = "http://mobile.tutsplus.com/tutorials/mobile-design-tutorials/80s-phone-app-slicing/";
		newDao.write(data, values);
		db.insert(TABLE_NAME, null, values);
	}

	public static void upgradeTableFrom(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
		final IDatabaseDao oldDao = daoForVersion(oldVersion);
		final IDatabaseDao newDao = daoForVersion(newVersion);

		List<DataModel> res = null;
		switch (oldVersion) {
			case DATABASE_VERSION_1:
				// Upgrade must be done using a particular sorting. The column we are sorting on might not be there for other DAO versions
				res = loadDataModel(db, SQL_GET_ALL, null, oldDao);
				updateToVersion2(res);
				/*
				 * .. updateProfileListToVersion3(res)... Place here the Profile
				 * update method for new Profile DB versions
				 */
				break;
			case DATABASE_VERSION_2:
				// do nothing since this is the last version
				break;
			default:
				break;
		}

		/* Drop the old table and create the new one. */
		newDao.ensureWritableTable(db, TABLE_NAME, true);

		saveDataModel(db, TABLE_NAME, res, newDao);

		newDao.ensureIndexes(db, TABLE_NAME);

	}

	private static void updateToVersion2(final List<DataModel> res) {
		for (final DataModel profile : res) {
			/* Add new option */
			profile.newData = true;
		}
	}

	private static List<DataModel> loadDataModel(final SQLiteDatabase db, final String sql, final String[] args, final IDatabaseDao dao) {

		final List<DataModel> res = new ArrayList<DataModel>();
		Cursor profileCursor = null;
		try {
			profileCursor = db.rawQuery(sql, args);
			if (profileCursor.moveToFirst()) {
				while (profileCursor.isAfterLast() == false) {
					final DataModel profile = dao.read(profileCursor);
					res.add(profile);
					profileCursor.moveToNext();
				}
			} else {
				/* record set was empty */
			}
		} finally {
			if (profileCursor != null) {
				profileCursor.close();
			}
		}

		return res;
	}

	private static void saveDataModel(final SQLiteDatabase db, final String tableName, final List<DataModel> res, final IDatabaseDao dao) {
		if (res.isEmpty()) {
			return;
		}

		try {
			db.beginTransaction();
			final ContentValues args = new ContentValues();
			for (final DataModel profile : res) {
				dao.write(profile, args);
				db.insert(tableName, null, args);
				args.clear();
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}
}