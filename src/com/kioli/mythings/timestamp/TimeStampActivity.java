package com.kioli.mythings.timestamp;

import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.betterComponents.BetterTextView;
import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;

public class TimeStampActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timestamp);
		init();
	}

	private void init() {
		try {
			ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), 0);
			ZipFile zf = new ZipFile(ai.sourceDir);
			ZipEntry ze = zf.getEntry("classes.dex");
			long time = ze.getTime();
			String build_timestamp = SimpleDateFormat.getInstance().format(new java.util.Date(time));
			BetterTextView version = (BetterTextView) findViewById(R.id.timestamp_text);
			version.setText(getString(R.string.time_stamp_txt, getString(R.string.app_version_number), build_timestamp));
		} catch (Exception e) {
			MyLog.e("TIMESTAMP ACTIVITY", "Failed to append version with debug info");
		}
	};
}