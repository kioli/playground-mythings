package com.kioli.mythings.slideMenu;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.slideMenu.MyHorizontalScrollView.SizeCallback;
import com.kioli.mythings.utils.MyLog;

/** based on: https://github.com/gitgrimbo/android-sliding-menu-demo TODO: This activity should be abstract
 *
 * @author Lorenzo */
public class SlideMenuActivity extends Activity {

//	private static final int SWIPE_MIN_DISTANCE = 3;
//	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
//	private static final int DIRECTION_RIGHT = 0;
//	private static final int DIRECTION_LEFT = 1;

	private MyHorizontalScrollView scrollView;
	private View menuLeft;
	private View main;
	private View menuRight;

	private TextView main_text;

	private ImageButton slideLeft;
	private ImageButton slideRight;

	private int buttonLeftWidth;
	private int buttonRightWidth;

	private enum SlideState {LEFT, MAIN, RIGHT};
	private SlideState currentState;

	private LayoutInflater inflater;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		inflater = LayoutInflater.from(this);
		scrollView = (MyHorizontalScrollView) inflater.inflate(R.layout.activity_horz_scroll, null);
		setContentView(scrollView);

		init();
	}

	private void init() {
//		scrollView.setOnTouchListener(swipeListener);
//		scrollView.setAllowSwipe(false);

		main = inflater.inflate(R.layout.horz_scroll_menu_central, null);
		menuLeft = inflater.inflate(R.layout.horz_scroll_menu_left, null);
		menuRight = inflater.inflate(R.layout.horz_scroll_menu_right, null);

		main_text = (TextView) main.findViewById(R.id.main_text);

		initListView((ListView) main.findViewById(R.id.list));
		initListView((ListView) menuLeft.findViewById(R.id.list));

		slideLeft = (ImageButton) main.findViewById(R.id.button1);
		slideRight = (ImageButton) main.findViewById(R.id.button2);

		slideLeft.setOnClickListener(listener);
		slideRight.setOnClickListener(listener);

		currentState = SlideState.MAIN;

		final View[] children = new View[] { menuLeft, main, menuRight };

		scrollView.initViews(children, 1, new SizeCallbackForMenu());
	}

	public void scrollToLeft() {
		currentState = SlideState.LEFT;
		scrollView.smoothScrollTo(0, 0);
	}

	public void scrollToMain() {
		currentState = SlideState.MAIN;
		scrollView.smoothScrollTo(menuLeft.getMeasuredWidth(), 0);
	}

	public void scrollToRight() {
		currentState = SlideState.RIGHT;
		scrollView.smoothScrollTo(main.getMeasuredWidth() + menuLeft.getMeasuredWidth() - slideRight.getMeasuredWidth(), 0);
	}

	/** Fills the example listview */
	private void initListView(ListView list) {
		String[] arr = new String[10];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = "Menu " + (i + 1);
		}
		list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arr));
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String msg = "item[" + position + "]=" + parent.getItemAtPosition(position);
				main_text.setText(msg);
				main_text.setVisibility(View.VISIBLE);
				((ListView) main.findViewById(R.id.list)).setVisibility(View.GONE);
				scrollToMain();
			}
		});
	}

	private OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			MyLog.i("Lorenzo", "onClick");
			switch (v.getId()) {
				case R.id.button1:
					if (currentState == SlideState.MAIN) {
						scrollToLeft();
					} else if (currentState == SlideState.LEFT) {
						scrollToMain();
					}
					break;
				case R.id.button2:
					if (currentState == SlideState.MAIN) {
						scrollToRight();
					} else if (currentState == SlideState.RIGHT) {
						scrollToMain();
					}
					break;
				default:
					break;
			}

		}
	};

//	private OnTouchListener swipeListener = new OnTouchListener() {
//		// TODO
//		private GestureDetector flingDetector = new GestureDetector(new MyGestureDetector());
//
//		@Override
//		public boolean onTouch(View v, MotionEvent event) {
//
//			if (flingDetector.onTouchEvent(event)) {
//				return true;
//			}
//
//			int scrollX = scrollView.getScrollX();
//			if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
//				MyLog.i("Lorenzo", "touch in ActionUP");
//
//				switch (currentState) {
//					case LEFT:
//						if (scrollX > menuLeft.getMeasuredWidth() / 2) {
//							scrollToMain();
//						} else {
//							scrollToLeft();
//						}
//						break;
//					case RIGHT:
//						if (scrollX < menuLeft.getMeasuredWidth() + main.getMeasuredWidth() / 2) {
//							scrollToMain();
//						} else {
//							scrollToRight();
//						}
//						break;
//					case MAIN:
//						if (scrollX <= menuLeft.getMeasuredWidth() / 2) {
//							scrollToLeft();
//						} else if (scrollX >= menuLeft.getMeasuredWidth() + main.getMeasuredWidth() / 2) {
//							scrollToRight();
//						} else {
//							scrollToMain();
//						}
//						break;
//					default:
//						break;
//				}
//				return true;
//			}
//			return false;
//		}
//	};

//	private class MyGestureDetector extends SimpleOnGestureListener {
//		@Override
//		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//			try {
//				if (!scrollView.isAllowSwipe()) {
//					return false;
//				}
//
//				int direction = -1;
//
//				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
//					direction = DIRECTION_RIGHT;
//				}
//
//				else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
//					direction = DIRECTION_LEFT;
//				}
//				if (direction != -1) {
//
//					switch (currentState) {
//						case LEFT:
//							if (direction == DIRECTION_RIGHT) {
//								scrollToMain();
//								MyLog.i("Lorenzo", "handled by fling1");
//							} else {
//								scrollToLeft();
//							}
//							break;
//						case MAIN:
//							if (direction == DIRECTION_RIGHT) {
//								scrollToRight();
//								MyLog.i("Lorenzo", "handled by fling2.1");
//
//							} else if (direction == DIRECTION_LEFT) {
//								scrollToLeft();
//								MyLog.i("Lorenzo", "handled by fling2.2");
//							} else {
//								scrollToMain();
//							}
//							break;
//						case RIGHT:
//							if (direction == DIRECTION_LEFT) {
//								scrollToMain();
//								MyLog.i("Lorenzo", "handled by fling3");
//							} else {
//								scrollToRight();
//							}
//							break;
//						default:
//							break;
//					}
//					return true;
//				}
//			} catch (Exception e) {
//				MyLog.e("Lorenzo", "There was an error processing the Fling event:" + e.getMessage());
//			}
//			MyLog.i("Lorenzo", "fling returned false");
//			return false;
//		}
//	}

	/** Helper that remembers the width of the 'slide' button, so that the 'slide' button remains in view, even when the
	 * menu is showing. */
	private class SizeCallbackForMenu implements SizeCallback {

		@Override
		public void onGlobalLayout() {
			buttonLeftWidth = slideLeft.getMeasuredWidth();
			buttonRightWidth = slideRight.getMeasuredWidth();

			// MyLog.i("Lorenzo", "main:" + mainWidth + " left:" + leftWidth + " right:" + rightWidth);

			System.out.println("btnWidth=" + buttonLeftWidth);
			System.out.println("btnWidth=" + buttonRightWidth);
		}

		@Override
		public void getViewSize(int idx, int w, int h, int[] dims) {
			dims[0] = w;
			dims[1] = h;
			if (idx == 0) {
				// left menu
				dims[0] = w - buttonLeftWidth;
			}
			if (idx == 2) {
				// right menu
				dims[0] = w - buttonRightWidth;
			}
		}
	}
}