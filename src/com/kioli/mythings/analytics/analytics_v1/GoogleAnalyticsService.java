package com.kioli.mythings.analytics.analytics_v1;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.constants.Constants;

/** Service that retains a single instance to GoogleAnalyticsTracker as long as there is at least one Activity bound to
 * it. IT keeps the session while the user goes around the app */
public class GoogleAnalyticsService extends Service {

	// Binder given to clients
	private final IBinder mBinder = new GABinder();
	private GoogleAnalyticsTracker tracker;

	/** When bound to the service it:
	 * - gets a tracker from GoogleAnalytics
	 * - starts it updating to Google every 30 seconds
	 * - checks over 100% of the visitors */
	@Override
	public IBinder onBind(Intent arg0) {
		MyLog.w(this.getClass().getName(), "Initial Activity bound to service");
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession(Constants.GOOGLE_ANALYTICS_ID, 30, getApplication());
		tracker.setSampleRate(100);
		return mBinder;
	}

	/** When unbound to the service it releases the tracker */
	@Override
	public boolean onUnbind(Intent intent) {
		MyLog.w(this.getClass().getName(), "App no more bound to service");
		tracker.dispatch();
		tracker.stopSession();
		return false;
	}

	/** Return this instance of GoogleAnalyticsService so clients can call public methods */
	public class GABinder extends Binder {
		public GoogleAnalyticsService getService() {
			return GoogleAnalyticsService.this;
		}
	}

	public GoogleAnalyticsTracker getTracker() {
		return tracker;
	}
}