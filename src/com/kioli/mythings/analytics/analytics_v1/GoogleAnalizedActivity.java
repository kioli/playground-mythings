package com.kioli.mythings.analytics.analytics_v1;

import java.util.Vector;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.kioli.mythings.R;
import com.kioli.mythings.analytics.analytics_v1.GoogleAnalyticsService.GABinder;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import com.kioli.mythings.utils.constants.Constants;

/** Class for setting up the google analytics for the entire application
 *
 * @see Note: needs the library libGoogleAnalytics.jar
 * @author Kioli */
public class GoogleAnalizedActivity extends BetterActivity {

	/** GoogleAnalytics stuff */
	private GoogleAnalyticsService analyticsService;
	private GoogleAnalyticsTracker tracker;
	private boolean mBound = false;
	private final Vector<String> pendingTrackingCalls = new Vector<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_analytics);
		init();
	}

	/** When the activity starts it binds to the service given by GoogleAnalyticsService.java */
	@Override
	protected void onStart() {
		super.onStart();
		Intent intent = new Intent(this, GoogleAnalyticsService.class);
		bindService(intent, analyticsConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mBound) {
			unbindService(analyticsConnection);
			mBound = false;
		}
	}

	public void init() {
		// Possible to do things
	}

	/** Either tracks this pageview immediately, or adds it to a Vector of views to be tracked once the Activity binds to
	 * the Service.
	 *
	 * @param page
	 *        String containing the location of the page to track */
	public void trackPageView(String page) {
		if (mBound) {
			tracker.trackPageView(page);
		} else {
			pendingTrackingCalls.add(page);
		}
	}

	/** Handles callbacks for GoogleAnalyticsService binding */
	private final ServiceConnection analyticsConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to the GoogleAnalyticsService
			mBound = true;
			MyLog.w("AnalizedActivityConnection", "service connected.");
			GABinder binder = (GABinder) service;
			analyticsService = binder.getService();
			tracker = analyticsService.getTracker();

			// take care of any pending tracking calls
			if (!pendingTrackingCalls.isEmpty()) {
				for (String page : pendingTrackingCalls) {
					tracker.trackPageView(page);
				}
				pendingTrackingCalls.clear();
			}
			trackPageView(Constants.ANALYTICS_ACTIVITY);
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			MyLog.w("AnalizedActivityConnection", "service disconnected.");
			mBound = false;
		}
	};
}