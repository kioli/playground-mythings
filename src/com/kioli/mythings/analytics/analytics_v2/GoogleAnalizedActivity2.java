package com.kioli.mythings.analytics.analytics_v2;

import android.os.Bundle;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

/**
 * Class using analytics V2
 *
 * @see Note: needs the library libGoogleAnalyticsV2.jar
 * @author Kioli
 */
public class GoogleAnalizedActivity2 extends BetterActivity {

	/** GoogleAnalytics stuff */
	private Tracker mGaTracker;
	private GoogleAnalytics mGaInstance;
	private final static String page = "/Analyzed_activity_2";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_analytics);

		// Get the GoogleAnalytics singleton using the application context to avoid leaking the current context.
		mGaInstance = GoogleAnalytics.getInstance(this);

		// Use the GoogleAnalytics singleton to get the Tracker defined in analytics.xml.
		mGaTracker = mGaInstance.getTracker(getString(R.string.ga_trackingId));
	}

	/** When the activity starts it tracks the page */
	@Override
	protected void onStart() {
		super.onStart();
		trackPageView(page);
	}

	/**
	 * Tracks this page-view immediately
	 *
	 * @param page
	 *        String containing the location of the page to track
	 */
	public void trackPageView(String page) {
		// Send a screen view when the Activity is displayed to the user.
		mGaTracker.sendView(page);
		mGaTracker.setAppName(getApplication().getPackageName());
	}
}