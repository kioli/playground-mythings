package com.kioli.mythings.multiLayoutList;

import java.util.ArrayList;
import com.kioli.mythings.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyCustomAdapter extends BaseAdapter {

	private static final int TYPE_BG1 = 0;
	private static final int TYPE_BG2 = 1;
	private static final int TYPE_MAX_COUNT = 2;

	private ArrayList<String> data;
	private Context context;

	public MyCustomAdapter(Context ctx, ArrayList<String> myData) {
		context = ctx;
		data = myData;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			int type = getItemViewType(position);
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			switch (type) {
				case TYPE_BG1:
					convertView = inflater.inflate(R.drawable.multi_list_row_1, parent, false);
					break;
				case TYPE_BG2:
					convertView = inflater.inflate(R.drawable.multi_list_row_2, parent, false);
					break;
			}
		}
		TextView textView = (TextView) convertView.findViewById(R.id.multi_list_row_text);
		textView.setText(data.get(position));
		return convertView;
	}

	/** Defines how many different layouts the list this adapter is used on has */
	@Override
	public int getViewTypeCount() {
		return TYPE_MAX_COUNT;
	}

	/** Checks which layout to use */
	@Override
	public int getItemViewType(int position) {
		if (position % 2 == 0) {
			return TYPE_BG1;
		} else {
			return TYPE_BG2;
		}
	}
}