package com.kioli.mythings.multiLayoutList;

import java.util.ArrayList;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MultiLayoutListActivity extends BetterActivity {

	private ListView list;
	private Context context;
	private MyCustomAdapter adapter;
	private ArrayList<String> data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_layout_list);
		context = this;
		init();
	}

	private void init() {
		list = (ListView) findViewById(R.id.list_multi_bg);
		data = new ArrayList<String>();
		for (int i = 1; i < 20; i++) {
			data.add("item " + i);
		}
		adapter = new MyCustomAdapter(context, data);
		list.setAdapter(adapter);
		list.setOnItemClickListener(listener);
	}

	private OnItemClickListener listener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Toast.makeText(context, "pressed item " + (position + 1), 200).show();
		}
	};
}