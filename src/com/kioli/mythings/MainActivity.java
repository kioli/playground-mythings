package com.kioli.mythings;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.crashlytics.android.Crashlytics;
import com.kioli.mythings.analytics.analytics_v1.GoogleAnalizedActivity;
import com.kioli.mythings.analytics.analytics_v2.GoogleAnalizedActivity2;
import com.kioli.mythings.asyncTaskLoader_serverCommunication.ServerCommunicationActivity;
import com.kioli.mythings.carousel.CarouselActivity;
import com.kioli.mythings.customizedXmlAttributes.AttrsActivity;
import com.kioli.mythings.db.MainListActivity;
import com.kioli.mythings.dbFromAsset.AutocompleteListActivity;
import com.kioli.mythings.dialog.ActivityDialog;
import com.kioli.mythings.fragments.ActivityFragment;
import com.kioli.mythings.interfaces.InterfaceActivity;
import com.kioli.mythings.multiLayoutList.MultiLayoutListActivity;
import com.kioli.mythings.multimediaPlay.MultimediaActivity;
import com.kioli.mythings.notification.NotificationActivity;
import com.kioli.mythings.openMap.OpenStreetMapActivity;
import com.kioli.mythings.parcelable.FirstParcelActivity;
import com.kioli.mythings.parseHTMLforWebview.ParseHTMLforWebviewActivity;
import com.kioli.mythings.pattern.PatternsActivity;
import com.kioli.mythings.setTypefaceProgrammatically.SetTypefaceActivity;
import com.kioli.mythings.slideMenu.SlideMenuActivity;
import com.kioli.mythings.socialSharing.SharingActivity;
import com.kioli.mythings.stringStyle.StringStyleActivity;
import com.kioli.mythings.timestamp.TimeStampActivity;
import com.kioli.mythings.uniqueID.UniqueIDActivity;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import com.kioli.mythings.utils.betterComponents.multifields.ActivityMultifields;

public class MainActivity extends BetterActivity {

	MainAdapter	      adapter;
	ListView	      list;
	ArrayList<String>	buttons;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.main);
		init();
	}

	public interface ICallback {
		public void onItemClicked(int button);
	}

	private void init() {
		list = (ListView) findViewById(R.id.main_list);

		buttons = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.main_array)));

		adapter = new MainAdapter(this, buttons);
		adapter.setCallback(listClickCallback);

		list.setAdapter(adapter);
	}

	ICallback	listClickCallback	= new ICallback() {
		                              @Override
		                              public void onItemClicked(final int position) {
			                              switch (position) {
											  case 0:
												  startActivity(new Intent(MainActivity.this, GoogleAnalizedActivity.class));
												  break;
											  case 1:
												  startActivity(new Intent(MainActivity.this, GoogleAnalizedActivity2.class));
												  break;
											  case 2:
												  startActivity(new Intent(MainActivity.this, UniqueIDActivity.class));
												  break;
											  case 3:
												  startActivity(new Intent(MainActivity.this, SetTypefaceActivity.class));
												  break;
											  case 4:
												  startActivity(new Intent(MainActivity.this, ParseHTMLforWebviewActivity.class));
												  break;
											  case 5:
												  startActivity(new Intent(MainActivity.this, SharingActivity.class));
												  break;
											  case 6:
												  startActivity(new Intent(MainActivity.this, ServerCommunicationActivity.class));
												  break;
											  case 7:
												  startActivity(new Intent(MainActivity.this, MultimediaActivity.class));
												  break;
											  case 8:
												  startActivity(new Intent(MainActivity.this, OpenStreetMapActivity.class));
												  break;
											  case 9:
												  startActivity(new Intent(MainActivity.this, FirstParcelActivity.class));
												  break;
											  case 10:
												  startActivity(new Intent(MainActivity.this, StringStyleActivity.class));
												  break;
											  case 11:
												  startActivity(new Intent(MainActivity.this, AutocompleteListActivity.class));
												  break;
											  case 12:
												  startActivity(new Intent(MainActivity.this, InterfaceActivity.class));
												  break;
											  case 13:
												  startActivity(new Intent(MainActivity.this, SlideMenuActivity.class));
												  break;
											  case 14:
												  startActivity(new Intent(MainActivity.this, MultiLayoutListActivity.class));
												  break;
											  case 15:
												  startActivity(new Intent(MainActivity.this, NotificationActivity.class));
												  break;
											  case 16:
												  startActivity(new Intent(MainActivity.this, AttrsActivity.class));
												  break;
											  case 17:
												  startActivity(new Intent(MainActivity.this, TimeStampActivity.class));
												  break;
											  case 18:
												  startActivity(new Intent(MainActivity.this, ActivityFragment.class));
												  break;
											  case 19:
												  startActivity(new Intent(MainActivity.this, ActivityDialog.class));
												  break;
											  case 20:
												  startActivity(new Intent(MainActivity.this, CarouselActivity.class));
												  break;
											  case 21:
												  startActivity(new Intent(MainActivity.this, PatternsActivity.class));
												  break;
											  case 22:
												  startActivity(new Intent(MainActivity.this, MainListActivity.class));
												  break;
											  case 23:
												  startActivity(new Intent(MainActivity.this, ActivityMultifields.class));
												  break;
											  default:
												  break;
										  }
									  }
	                              };
}