package com.kioli.mythings.parcelable;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import com.kioli.mythings.utils.constants.Constants;

public class FirstParcelActivity extends BetterActivity {

	private String name = "Lorenzo";
	private String surname = "Marchiori";
	private int age = 27;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parcelable_first);
		init();
	}

	private void init() {
		findViewById(R.id.btn_send_parcel).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// Create an array of parcelable objects
				ParcelableObject[] arr = (ParcelableObject[]) new ParcelableObject().CREATOR.newArray(2);

				for (int i = 0; i < arr.length; i++) {
					ParcelableObject obj = new ParcelableObject();
					obj.setName(name);
					obj.setSurname(surname);
					obj.setAge(age);
					arr[i] = obj;
				}

				// Create a parcelable object
//				ParcelableObject obj = new ParcelableObject();
//				obj.setName(name);
//				obj.setSurname(surname);
//				obj.setAge(age);

				Intent intent = new Intent(FirstParcelActivity.this, SecondParcelActivity.class);
				intent.putExtra(Constants.PARCELABLE_INTENT, arr);
//				intent.putExtra(Constants.PARCELABLE_INTENT, obj);

				startActivity(intent);
			}
		});

		findViewById(R.id.btn_back_parcel).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}