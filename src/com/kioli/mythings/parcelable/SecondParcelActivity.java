package com.kioli.mythings.parcelable;

import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import com.kioli.mythings.utils.constants.Constants;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class SecondParcelActivity extends BetterActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parcelable_second);
		init();
	}

	private void init() {
		TextView first = (TextView) findViewById(R.id.parcel_first_outcome);
		TextView second = (TextView) findViewById(R.id.parcel_second_outcome);
		TextView third = (TextView) findViewById(R.id.parcel_third_outcome);

		Bundle b = getIntent().getExtras();

		// Read the parcelable object from the intent
//		ParcelableObject obj = b.getParcelable(Constants.PARCELABLE_INTENT);

		// Get the content of the object
//		first.setText(obj.getName());
//		second.setText(obj.getSurname());
//		third.setText(String.valueOf(obj.getAge()));

		// Read the array of parcelables
		Parcelable[] arr = (Parcelable[]) b.getParcelableArray(Constants.PARCELABLE_INTENT);

		String name = "", surname = "", age = "";
		for (int i = 0; i < arr.length ; i++) {
			ParcelableObject obj = (ParcelableObject) arr[i];
			name += obj.getName() + " - ";
			surname += obj.getSurname() + " - ";
			age += obj.getAge() + " - ";
		}

		first.setText(name);
		second.setText(surname);
		third.setText(age);

		findViewById(R.id.btn_back_parcel).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}