package com.kioli.mythings.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/** A parcelable object is needed when you want to pass through intent something more complicated than a string or an int */
public class ParcelableObject implements Parcelable {

	private String name;
	private String surname;
	private int age;

	/** Standard basic constructor for non-parcel object creation */
	public ParcelableObject() {
	}

	/** Constructor to use when re-constructing object from a parcel *
	 *
	 * @param in
	 *        a parcel from which to read this object */
	public ParcelableObject(Parcel in) {
		readFromParcel(in);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	/** Writes the data in the parcel and when read they will come back in the same order */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(surname);
		dest.writeInt(age);
	}

	/** Called from the constructor to create this object from a parcel.
	 *
	 * @param in
	 *        parcel from which to re-create object */
	private void readFromParcel(Parcel in) {
		name = in.readString();
		surname = in.readString();
		age = in.readInt();
	}

	/** This field is needed for Android to be able to create new objects, individually or as arrays. This also means
	 * that you can use use the default constructor to create the object. */
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public ParcelableObject createFromParcel(Parcel in) {
			return new ParcelableObject(in);
		}

		public ParcelableObject[] newArray(int size) {
			return new ParcelableObject[size];
		}
	};
}