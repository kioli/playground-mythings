package com.kioli.mythings.openMap;

import java.util.ArrayList;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.provider.Settings;
import android.view.Window;
import android.widget.Toast;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.constants.Constants;

/** Class to show an Open Street Map
 *
 * @see Note 1: This does not extend BetterActivity but only Activity to avoid problems with setContentView
 * @see Note 2: needs the librearies osmdroid-android.jar and slf4j-android.jar
 * @author M2Mobi */

public class OpenStreetMapActivity extends Activity {

	private MapView myOpenMapView;
	private MapController myMapController;

	private LocationManager myLocationManager;
	private LocationListener myLocationListener;
	private String bestProvider;
	private GeoPoint myGeoPoint;

	private MyMapOverlay myLocationOverlay;
	ArrayList<OverlayItem> myOverlayItemArray;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_map);
		init();
	}

	private void init() {
		myOpenMapView = (MapView) findViewById(R.id.openmapview);
		myOpenMapView.setBuiltInZoomControls(true);
		myMapController = myOpenMapView.getController();
		myMapController.setZoom(12);

		// Add Scale Bar
		myOpenMapView.getOverlays().add(new ScaleBarOverlay(this));

		myLocationListener = new MyLocationListener(handleChangePosition);

		myLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		bestProvider = myLocationManager.getBestProvider(new Criteria(), true);

		// Check if enabled and if not send user to the GSP settings
		if (bestProvider == null) {
			showGPSDialog();
		} else {
			showMap();
		}

		// --- Create Another Overlay for multi marker
		myOverlayItemArray = new ArrayList<OverlayItem>();
		myOverlayItemArray.add(new OverlayItem("Amsterdam", "office", myGeoPoint));
		ItemizedIconOverlay<OverlayItem> anotherItemizedIconOverlay = new ItemizedIconOverlay<OverlayItem>(this, myOverlayItemArray, null);
		myOpenMapView.getOverlays().add(anotherItemizedIconOverlay);
	}

	private void centerMap(GeoPoint centerGeoPoint) {
		myMapController.animateTo(centerGeoPoint);

		String lon = getResources().getString(R.string.map_long_str);
		String lat = getResources().getString(R.string.map_lat_str);

		Toast.makeText(OpenStreetMapActivity.this,
				lon + ": " + centerGeoPoint.getLongitudeE6() + " - " + lat + ": " + centerGeoPoint.getLatitudeE6(), Toast.LENGTH_SHORT).show();
	}

	private Handler handleChangePosition = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			centerMap((GeoPoint) msg.obj);
		};
	};

	private void showGPSDialog() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(OpenStreetMapActivity.this);
		alertDialog.setTitle(getResources().getString(R.string.map_gps_dialog_title));
		alertDialog.setMessage(getResources().getString(R.string.map_gps_dialog_text));
		alertDialog.setPositiveButton(getResources().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivityForResult(intent, Constants.GPS_REQUESTCODE);
			}
		});
		alertDialog.setNegativeButton(getResources().getString(android.R.string.no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alertDialog.show();
	}

	private void showMap() {
		myLocationManager.requestLocationUpdates(bestProvider, 0, 1, myLocationListener);

		Location location = myLocationManager.getLastKnownLocation(bestProvider);
		if (location != null) {
			myGeoPoint = new GeoPoint((int) (myLocationManager.getLastKnownLocation(bestProvider).getLatitude() * 1000000), (int) (myLocationManager
					.getLastKnownLocation(bestProvider).getLongitude() * 1000000));

			centerMap(myGeoPoint);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.GPS_REQUESTCODE) {
			bestProvider = myLocationManager.getBestProvider(new Criteria(), true);
			showMap();
		}
	}
}