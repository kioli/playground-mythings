package com.kioli.mythings.openMap;

import org.osmdroid.util.GeoPoint;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class MyLocationListener implements LocationListener {

	GeoPoint myGeoPoint;
	Handler handler;

	MyLocationListener(Handler hand) {
		handler = hand;
	}

	public void onProviderDisabled(String provider) {
	}

	public void onProviderEnabled(String provider) {
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void onLocationChanged(Location location) {
		myGeoPoint = new GeoPoint((int) (location.getLatitude() * 1000000), (int) (location.getLongitude() * 1000000));

		Message msg = new Message();
		msg.obj = myGeoPoint;
		handler.sendMessage(msg);
	}
}