package com.kioli.mythings.openMap;

import java.util.ArrayList;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapView;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;

public class MyMapOverlay extends ItemizedOverlay {

	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	private Context context;

	// public MyMapOverlay(Drawable marker, Context ctx) {
	// mOverlays.get(0).setMarkerHotspot(HotspotPlace.BOTTOM_CENTER);
	// context = ctx;
	// }

	public MyMapOverlay(Drawable marker, ResourceProxy proxy) {
		super(marker, proxy);
	}

	@Override
	public boolean onSnapToItem(int arg0, int arg1, Point arg2, IMapView arg3) {
		return false;
	}

	@Override
	protected OverlayItem createItem(int index) {
		return mOverlays.get(index);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	public void addOverlay(OverlayItem overlayItem) {
		mOverlays.add(overlayItem);

		// calls createItem to retrieve each OverlayItem
		populate();
	}
}