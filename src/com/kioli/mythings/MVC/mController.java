package com.kioli.mythings.MVC;


public class mController implements IController {

    private final MyLoader    loader;
    
    public mController(final mView activity, final int loaderIdBase) {
    	loader  = new MyLoader(activity);
    }
    
	@Override
	public void onClickedLoaderButton() {
        //TODO: Start spinner
        loader.startLoading();
	}
}
