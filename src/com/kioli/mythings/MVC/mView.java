package com.kioli.mythings.MVC;

import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

public class mView extends FragmentActivity implements IController, LoaderCallbacks<SampleItem>, Observer {

	private static final class Controls {
		public TextView text;
		public Button button;
	}

	private Controls controls;
	private mController controller;
	private mModel model;
	private MyLoader loader;
	private int loaderID = 101;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mvc);

		initControls();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		getSupportLoaderManager().initLoader(loaderID, null, this);
	}

	private void initControls() {
		controls = new Controls();
		controls.button = (Button) findViewById(R.id.mvc_btn);
		controls.text = (TextView) findViewById(R.id.mvc_text);

		controls.button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickedLoaderButton();
			}
		});
	}

	@Override
	public void onClickedLoaderButton() {
		controller.onClickedLoaderButton();
	}

	@Override
	public Loader<SampleItem> onCreateLoader(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLoadFinished(Loader<SampleItem> arg0, SampleItem arg1) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void onLoaderReset(Loader<SampleItem> arg0) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void update(Observable observable, Object data) {
		// TODO Auto-generated method stub		
	}
}