package com.kioli.mythings.MVC;

import java.util.Observable;

public class SampleItem extends Observable {
 
    private int home_score = 0;
    private int visitor_score = 0;
 
    public SampleItem() {}
 
    // Grant 1 point to the team
    public void fieldGoal(int team) { 
        switch (team) {
            case 1:
                this.home_score = this.home_score + 1;
                break; 
            case 2:
                this.visitor_score = this.visitor_score + 1;
                break;
        } 
        triggerObservers(); 
    }
 
	// Returns the score of the Home or Visitor teams
	public int getScore(int team) {
		switch (team) {
		case 1:
			return home_score;
		case 2:
			return visitor_score;
		default:
			return 0;
		}
	}
 
    // Adds six points to score of the team
    public void touchDown(int team) { 
        switch (team) {
            case 1:
                this.home_score = this.home_score + 6;
                break; 
            case 2:
                this.visitor_score = this.visitor_score + 6;
                break;
        } 
        triggerObservers();
    }
 
    // Create a method to update the Observerable's flag to true for changes and
    // notify the observers to check for a change.
    private void triggerObservers() { 
        setChanged();
        notifyObservers();
    }
}
