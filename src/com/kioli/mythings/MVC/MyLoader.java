package com.kioli.mythings.MVC;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;


public class MyLoader extends AsyncTaskLoader<List<SampleItem>> {
	 
	  // We hold a reference to the Loader�s data here.
	  private List<SampleItem> mData;	  
	  
	  // The observer could be anything so long as it is able to detect content changes
	  // and report them to the loader with a call to onContentChanged(). For example,
	  // if you were writing a Loader which loads a list of all installed applications
	  // on the device, the observer could be a BroadcastReceiver that listens for the
	  // ACTION_PACKAGE_ADDED intent, and calls onContentChanged() on the particular 
	  // Loader whenever the receiver detects that a new application has been installed.
	  private Observer mObserver;
	 
	  public MyLoader(Context ctx) {
	    // Loaders may be used across multiple Activities, so NEVER hold a reference to the context
	    // directly.
	    super(ctx);
	  }
	 
	  @Override
	  public List<SampleItem> loadInBackground() {
	    List<SampleItem> data = new ArrayList<SampleItem>();
	 
	    // TODO: Perform the query here and add the results to 'data'.
	 
	    return data;
	  }
	 
	  @Override
	  public void deliverResult(List<SampleItem> data) {
	    if (isReset()) {
	      releaseResources(data);
	      return;
	    }
	 
	    // Hold a reference to the old data so it doesn't get garbage collected.
	    List<SampleItem> oldData = mData;
	    mData = data;
	 
	    if (isStarted()) {
	      super.deliverResult(data);
	    }
	 
	    // Invalidate the old data as we don't need it any more.
	    if (oldData != null && oldData != data) {
	      releaseResources(oldData);
	    }
	  }

	  @Override
	  protected void onStartLoading() {
	    if (mData != null) {
	      deliverResult(mData);
	    }
	 
	    // Begin monitoring the underlying data source.
	    if (mObserver == null) {
	      mObserver = (mView) getContext();
	      // TODO: register the observer
	    }
	 
	    if (takeContentChanged() || mData == null) {
	      // When the observer detects a change, it should call onContentChanged()
	      // on the Loader, which will cause the next call to takeContentChanged()
	      // to return true. If this is ever the case (or if the current data is
	      // null), we force a new load.
	      forceLoad();
	    }
	  }
	 
	  @Override
	  protected void onStopLoading() {
	    // The Loader is in a stopped state, so we should attempt to cancel the 
	    // current load (if there is one).
	    cancelLoad();
	 
	    // Note that we leave the observer as is. Loaders in a stopped state
	    // should still monitor the data source for changes so that the Loader
	    // will know to force a new load if it is ever started again.
	  }
	 
	  @Override
	  protected void onReset() {
	    // Ensure the loader has been stopped.
	    onStopLoading();
	 
	    // At this point we can release the resources associated with 'mData'.
	    if (mData != null) {
	      releaseResources(mData);
	      mData = null;
	    }
	 
	    // The Loader is being reset, so we should stop monitoring for changes.
	    if (mObserver != null) {
	      // TODO: unregister the observer
	      mObserver = null;
	    }
	  }
	 
	  @Override
	  public void onCanceled(List<SampleItem> data) {
	    // Attempt to cancel the current asynchronous load.
	    super.onCanceled(data);
	 
	    // The load has been canceled, so we should release the resources
	    // associated with 'data'.
	    releaseResources(data);
	  }
	 
	  private void releaseResources(List<SampleItem> data) {
	    // For a simple List, there is nothing to do. For something like a Cursor, we 
	    // would close it in this method.
	  }
	}