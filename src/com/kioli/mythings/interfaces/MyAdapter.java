package com.kioli.mythings.interfaces;

import com.kioli.mythings.R;
import com.kioli.mythings.dbFromAsset.tables.Table_countries.Table_countriesAll;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyAdapter extends CursorAdapter {

	Context context;
	String cursor_text;

	public MyAdapter(Context ctx, Cursor c) {
		super(ctx, c);
		context = ctx;
		cursor_text = context.getResources().getString(R.string.contry_name_column);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {

		int autocomplete = cursor.getColumnIndex(cursor_text);

		TextView textAirport = (TextView) view.findViewById(R.id.auto_list_text);
		textAirport.setText(cursor.getString(autocomplete));
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cursorView = vi.inflate(R.layout.view_myview_list_row_layout, null);
		return cursorView;
	}

	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
		// return MidSchipAPI.getInstance(mContext).dbAdapter.getAirlinesAndAirports(constraint);
		return context.getContentResolver().query(Table_countriesAll.TABLE_URI,
				new String[] { Table_countriesAll.ID, Table_countriesAll.COUNTRY_NAME },
				context.getResources().getString(R.string.contry_name_column) + " LIKE '" + constraint + "%'", null, null);
	}
}