package com.kioli.mythings.interfaces;

import com.kioli.mythings.R;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class InterfaceActivity extends Activity {

	MyView myview;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interface);
		context = this;
		init();
	}

	private void init() {
		findViewById(R.id.interface_button).setOnClickListener(listener);
		myview = (MyView) findViewById(R.id.search_bar);
		myview.setMyCallBack(listClickCallback);
	}

	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			findViewById(R.id.interface_button).setVisibility(View.GONE);
			myview.showView();
		}
	};

	MyInterface listClickCallback = new MyInterface() {
		@Override
		public void onItemSelected(String text) {
			Toast.makeText(context, "pressed " + text, Toast.LENGTH_SHORT).show();
		}
	};
}