package com.kioli.mythings.interfaces;

import android.content.Context;
import android.database.Cursor;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.dbFromAsset.tables.Table_countries.Table_countriesAll;

public class MyView extends RelativeLayout {

	private AutoCompleteTextView editSearch;
	private ListView listView;
	private MyInterface callback;
	private MyAdapter adapter;
	private Context context;

	/** Basic constructor inflating the layout for its view */
	public MyView(Context ctx) {
		super(ctx);
		context = ctx;
		View.inflate(context, R.layout.view_myview, this);
		init();
	}

	public MyView(Context ctx, AttributeSet attrs) {
		super(ctx, attrs);
		context = ctx;
		View.inflate(context, R.layout.view_myview, this);
		init();
	}

	/** Initialize the screen */
	private void init() {
		editSearch = (AutoCompleteTextView) findViewById(R.id.myview_edittext);
		editSearch.setText("");
		editSearch.requestFocus();
		editSearch.setThreshold(1);
		editSearch.addTextChangedListener(watchText);

		Cursor c = context.getContentResolver().query(Table_countriesAll.TABLE_URI, null, null, null, null);
		adapter = new MyAdapter(context, null);

		listView = (ListView) findViewById(R.id.myview_list);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(listListener);
	}

	/** Called to set the callback */
	public void setMyCallBack(MyInterface callback) {
		this.callback = callback;
	}

	/** TextWatcher to check on inserted text */
	private TextWatcher watchText = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			if (s.length() <= 0) {
				listView.setVisibility(View.INVISIBLE);
			} else {
				listView.setVisibility(View.VISIBLE);
			}
			adapter.getFilter().filter(s);
			adapter.notifyDataSetChanged();
		}
	};

	/** Listener to get clicked list items */
	private OnItemClickListener listListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> a, View view, int position, long id) {
			TextView txt = (TextView) view.findViewById(R.id.auto_list_text);
			if (callback != null) {
				callback.onItemSelected(txt.getText().toString());
			}
		}
	};

	public void showView() {
		MyView.this.setVisibility(View.VISIBLE);
	}
}
