package com.kioli.mythings;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kioli.mythings.MainActivity.ICallback;

public class MainAdapter extends BaseAdapter {

	private List<String> data;
	private Context context;
	private ViewHolder holder;
	private ICallback callback;

	public MainAdapter(Context ctx, List<String> myData) {
		context = ctx;
		data = myData;
	}

	private static class ViewHolder {
		TextView buttonTxt;
	}
	
	public void setCallback(ICallback callback) {
		this.callback = callback;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.main_list_row, parent,
					false);
			holder = new ViewHolder();
			holder.buttonTxt = (TextView) v.findViewById(R.id.main_list_text);
			v.setTag(holder);
		}
		
		holder = (ViewHolder) v.getTag();
		holder.buttonTxt.setText(data.get(position));
		
		v.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				callback.onItemClicked(position);
			}
		});
		
		return v;
	}
}