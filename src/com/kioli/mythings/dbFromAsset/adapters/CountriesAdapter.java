package com.kioli.mythings.dbFromAsset.adapters;

import com.kioli.mythings.R;
import com.kioli.mythings.dbFromAsset.tables.Table_countries.Table_countriesAll;
import com.kioli.mythings.utils.MyLog;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class CountriesAdapter extends CursorAdapter {

	int index_country_codeCol;
	int index_country_nameCol;
	String country_code_Col;
	String country_name_Col;
	Context context;

	public CountriesAdapter(Context ctx, Cursor c, String src, String disp) {
		super(ctx, c);

		context = ctx;
		country_code_Col = src;
		country_name_Col = disp;

		index_country_codeCol = c.getColumnIndex(country_code_Col);
		index_country_nameCol = c.getColumnIndex(country_name_Col);
	}

	/** Decides where in the layout to show the content of the DB it retrieves through the cursor */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// Set country code text
		TextView country_code = (TextView) view.findViewById(R.id.autocomplete_country_code);
		country_code.setText(cursor.getString(index_country_codeCol));

		// Set English name
		TextView country_name = (TextView) view.findViewById(R.id.autocomplete_country_name);
		country_name.setText(cursor.getString(index_country_nameCol));
	}

	/** It defines what to use as a layout for the autocompletition */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return LayoutInflater.from(context).inflate(R.drawable.activity_content_provider_countries_row, null);
	}

	/** Method that gives back to the cursor (and then written in the editText) the chosen entry from the drop-don list */
	@Override
	public String convertToString(Cursor cursor) {
		return cursor.getString(index_country_nameCol);
	}

	/** Needed to query every time the DB according to th new character written in the editText bar */
	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
		Cursor cursor = context.getContentResolver().query(Table_countriesAll.TABLE_URI,
				new String[] { Table_countriesAll.ID, Table_countriesAll.COUNTRY_CODE, Table_countriesAll.COUNTRY_NAME },
				country_code_Col + " LIKE '" + constraint + "%' OR " + country_name_Col + " LIKE '" + constraint + "%'", null, null);

		MyLog.e("QUERY", country_code_Col + " LIKE '" + constraint + "%' OR " + country_name_Col + " LIKE '" + constraint + "%'");
		index_country_nameCol = cursor.getColumnIndex(country_name_Col);
		index_country_codeCol = cursor.getColumnIndex(country_code_Col);

		return cursor;
	}
}