package com.kioli.mythings.dbFromAsset.adapters;

import com.kioli.mythings.R;
import com.kioli.mythings.dbFromAsset.tables.Table_eng_chi_airports.Table_eng_chi_airportsAll;
import com.kioli.mythings.utils.MyLog;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class AirportsAdapter extends CursorAdapter {

	int index_chinese_Col;
	int index_english_Col;
	String airport_chinese_Col;
	String airport_english_Col;
	Context context;

	public AirportsAdapter(Context ctx, Cursor c, String src, String disp) {
		super(ctx, c);

		context = ctx;
		airport_chinese_Col = src;
		airport_english_Col = disp;

		index_chinese_Col = c.getColumnIndex(airport_chinese_Col);
		index_english_Col = c.getColumnIndex(airport_english_Col);
	}

	/** Decides where in the layout to show the content of the DB it retrieves through the cursor */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// Set Chinese name
		TextView airport_chinese = (TextView) view.findViewById(R.id.autocomplete_airport_chinese);
		airport_chinese.setText(cursor.getString(index_chinese_Col));

		// Set English name
		TextView airport_english = (TextView) view.findViewById(R.id.autocomplete_airport_english);
		airport_english.setText(cursor.getString(index_english_Col));
	}

	/** It defines what to use as a layout for the autocompletition */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return LayoutInflater.from(context).inflate(R.drawable.activity_content_provider_airports_row, null);
	}

	/** Method that gives back to the cursor (and then written in the editText) the chosen entry from the drop-don list */
	@Override
	public String convertToString(Cursor cursor) {
		return cursor.getString(index_chinese_Col);
	}

	/** Needed to query every time the DB according to th new character written in the editText bar */
	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
		Cursor cursor = context.getContentResolver().query(Table_eng_chi_airportsAll.TABLE_URI,
				new String[] { Table_eng_chi_airportsAll.ID, Table_eng_chi_airportsAll.ENGLISH_NAME, Table_eng_chi_airportsAll.CHINESE_NAME },
				airport_chinese_Col + " LIKE '" + constraint + "%' OR " + airport_english_Col + " LIKE '" + constraint + "%'", null, null);

		MyLog.e("QUERY", airport_chinese_Col + " LIKE '" + constraint + "%' OR " + airport_english_Col + " LIKE '" + constraint + "%'");
		index_english_Col = cursor.getColumnIndex(airport_english_Col);
		index_chinese_Col = cursor.getColumnIndex(airport_chinese_Col);

		return cursor;
	}
}