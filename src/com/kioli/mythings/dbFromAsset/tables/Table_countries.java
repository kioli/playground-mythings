package com.kioli.mythings.dbFromAsset.tables;

import android.net.Uri;
import android.provider.BaseColumns;

import com.kioli.mythings.dbFromAsset.MyContentProvider;

public class Table_countries implements BaseColumns {

	public Table_countries() {
		// Empty
	}

	public static final class Table_countriesAll implements BaseColumns {

		// Name of the table as defined in the asset/sql/table.js file
		public static final String TABLE_NAME = "countries";

		// can be whatever (I guess)
		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.myThings.countries";

		// path to reach the table from where to get the DB
		public static final Uri		TABLE_URI		= Uri.parse("content://" + MyContentProvider.AUTHORITY + "/" + TABLE_NAME);

		// Name of the columns in the table as defined in the asset/sql/table.js file
		public static final String ID = "rowid as _id";
		public static final String COUNTRY_CODE = "country_code";
		public static final String COUNTRY_NAME = "country_name";
	}
}
