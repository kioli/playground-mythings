package com.kioli.mythings.dbFromAsset;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.widget.AutoCompleteTextView;

import com.kioli.mythings.R;
import com.kioli.mythings.dbFromAsset.adapters.AirportsAdapter;
import com.kioli.mythings.dbFromAsset.adapters.CountriesAdapter;
import com.kioli.mythings.dbFromAsset.tables.Table_countries.Table_countriesAll;
import com.kioli.mythings.dbFromAsset.tables.Table_eng_chi_airports.Table_eng_chi_airportsAll;

/** ContentProviderActivity class: Activity that contains the editText and creates a cursor with the information from the
 * DB. Then uses the adapter on the editText to properly populate it */
public class AutocompleteListActivity extends Activity {

	private AutoCompleteTextView autocompleteEditSearch;
	private Context context;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_content_provider);
		context = this;
		init();

		// Choose from which table to search
		//setAirportSearch();
		setCountrySearch();
	}

	private void init() {
		autocompleteEditSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete);
		autocompleteEditSearch.setFocusableInTouchMode(true);
		autocompleteEditSearch.setThreshold(1);
		autocompleteEditSearch.addTextChangedListener(textWatcher);
	}

	TextWatcher textWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
		}

		@Override
		public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
		}

		@Override
		public void afterTextChanged(final Editable s) {
			                        if (s.toString().matches("^[a-zA-Z]*$") == false) {
				                        final String error = getResources().getString(R.string.autocomplete_error);
				                        final ForegroundColorSpan fgcspan = new ForegroundColorSpan(R.color.red);
				                        final SpannableStringBuilder ssbuilder = new SpannableStringBuilder(error);
				                        ssbuilder.setSpan(fgcspan, 0, error.length(), 0);
				                        autocompleteEditSearch.setError(ssbuilder);
			}
		}
	};

	public void setAirportSearch() {
		// Name of the column in the DB table
		final String chineseCol = getString(R.string.airport_chinese_column);
		final String englishCol = getString(R.string.airport_eniglish_column);

		// Cursor to get DB entries
		final Cursor cursor = context.getContentResolver().query(Table_eng_chi_airportsAll.TABLE_URI,
				new String[] { Table_eng_chi_airportsAll.ID, Table_eng_chi_airportsAll.ENGLISH_NAME, Table_eng_chi_airportsAll.CHINESE_NAME }, null,
				null, null);

		// Adapter to populate the defined layout with the curson's contents
		final AirportsAdapter adapter = new AirportsAdapter(this, cursor, chineseCol, englishCol);
		autocompleteEditSearch.setAdapter(adapter);
	}

	public void setCountrySearch() {
		// Name of the column in the DB table
		final String countryCodeCol = getString(R.string.country_code_column);
		final String countryNameCol = getString(R.string.contry_name_column);

		// Cursor to get DB entries
		final Cursor cursor = context.getContentResolver().query(Table_countriesAll.TABLE_URI,
				new String[] { Table_countriesAll.ID, Table_countriesAll.COUNTRY_CODE, Table_countriesAll.COUNTRY_NAME }, null, null, null);

		// Adapter to populate the defined layout with the curson's contents
		final CountriesAdapter adapter = new CountriesAdapter(this, cursor, countryCodeCol, countryNameCol);
		autocompleteEditSearch.setAdapter(adapter);
	}
}
