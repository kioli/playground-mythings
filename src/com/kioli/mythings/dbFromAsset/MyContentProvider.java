package com.kioli.mythings.dbFromAsset;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.kioli.mythings.dbFromAsset.tables.Table_countries.Table_countriesAll;
import com.kioli.mythings.dbFromAsset.tables.Table_eng_chi_airports.Table_eng_chi_airportsAll;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.constants.Constants;

/** Content provider class: Used to communicate with the database (created from asset/sql/...) regarding
 * insertion/extraction and deletion of data. It specifies the reference to the different DB tables */
public class MyContentProvider extends ContentProvider {

	public static String AUTHORITY;
	private static UriMatcher uriMatcher;
	private DBHelper dbHelper;

	@Override
	public boolean onCreate() {
		AUTHORITY = getContext().getPackageName() + ".MyContentProvider";

		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, Table_eng_chi_airportsAll.TABLE_NAME, Constants.TABLE_ENG_CHI_AIRPORTS);
		uriMatcher.addURI(AUTHORITY, Table_countriesAll.TABLE_NAME, Constants.TABLE_COUNTRIES);

		dbHelper = DBHelper.getInstance(getContext());
		dbHelper.getWritableDatabase();
		return true;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		MyLog.i("DATABASE", "database delete");

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int count;

		switch (uriMatcher.match(uri)) {
			case Constants.TABLE_ENG_CHI_AIRPORTS:
				count = db.delete(Table_eng_chi_airportsAll.TABLE_NAME, selection, selectionArgs);
				break;
			case Constants.TABLE_COUNTRIES:
				count = db.delete(Table_countriesAll.TABLE_NAME, selection, selectionArgs);
				break;
			default:
				throw new IllegalArgumentException(Constants.EXCEPTION_URI_UNKNOWN + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
			case Constants.TABLE_ENG_CHI_AIRPORTS:
				return Table_eng_chi_airportsAll.CONTENT_TYPE;
			case Constants.TABLE_COUNTRIES:
				return Table_countriesAll.CONTENT_TYPE;
			default:
				throw new IllegalArgumentException(Constants.EXCEPTION_URI_NOT_SUPPORTED + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) throws SQLException {
		MyLog.i("DATABASE", "database insert");

		if (values != null) {
			SQLiteDatabase db = null;
			try {
				db = this.dbHelper.getWritableDatabase();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (db != null) {
				db.beginTransaction();
				try {
					db.insert(uri.getLastPathSegment(), null, values);
					db.setTransactionSuccessful();
				} catch (Exception e) {
					e.printStackTrace();
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
				} finally {
					db.endTransaction();
				}
			}
		}
		return uri;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		MyLog.i("DATABASE", "database queried");
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		switch (uriMatcher.match(uri)) {
			case Constants.TABLE_ENG_CHI_AIRPORTS:
				qb.setTables(Table_eng_chi_airportsAll.TABLE_NAME);
				break;
			case Constants.TABLE_COUNTRIES:
				qb.setTables(Table_countriesAll.TABLE_NAME);
				break;
			default:
				throw new IllegalArgumentException(Constants.EXCEPTION_URI_UNKNOWN + uri);
		}
		SQLiteDatabase db = this.dbHelper.getWritableDatabase();
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);

		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {
		MyLog.i("DATABASE", "database BULK insert");

		if (values != null) {
			SQLiteDatabase db = null;
			try {
				db = this.dbHelper.getWritableDatabase();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (db != null) {
				db.beginTransaction();
				try {
					for (ContentValues cv : values) {
						db.replace(uri.getLastPathSegment(), null, cv);
					}

					db.setTransactionSuccessful();
				} catch (Exception e) {
					e.printStackTrace();
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
				} finally {
					db.endTransaction();
				}
			}
		}
		return values.length;
	}
}