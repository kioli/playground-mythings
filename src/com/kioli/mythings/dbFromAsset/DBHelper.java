package com.kioli.mythings.dbFromAsset;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.constants.Constants;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/** Class that creates the DataBase starting from the queries written in the files present in the folder assets */

public class DBHelper extends SQLiteOpenHelper {

	private static String DB_NAME;
	Context mContext;
	private static DBHelper instance;

	private DBHelper(Context context, String databaseName) {
		super(context, databaseName, null, Constants.DB_VERSION);
		this.mContext = context;
		instance = this;
	}

	/** Return the instance of DBHelper */
	public static DBHelper getInstance(Context context) {

		DB_NAME = context.getPackageName().replace(".", "");

		if (instance == null)
			instance = new DBHelper(context, DB_NAME);
		return instance;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// Get all sql files from assets
		AssetManager assetManager = this.mContext.getAssets();
		String files[] = null;
		BufferedReader reader;
		String line;

		try {
			files = assetManager.list("sql/create");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (files != null && files.length > 0) {
			db.beginTransaction();
			for (String filename : files) {
				try {
					reader = new BufferedReader(new InputStreamReader(assetManager.open("sql/create/" + filename)));

					while ((line = reader.readLine()) != null) {
						if (line.compareTo("") != 0 && line.length() > 1) {
							db.execSQL(line);
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}

		MyLog.e("DATABASE", "database created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		MyLog.e("DATABASE", "Upgrading database from version " + oldVersion + " to " + newVersion);

		// Get all sql file from assets
		AssetManager assetManager = this.mContext.getAssets();
		String files[] = null;
		BufferedReader reader;
		String line;

		try {
			files = assetManager.list("sql/upgrade");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (files != null && files.length > 0) {
			db.beginTransaction();
			for (String filename : files) {
				try {
					reader = new BufferedReader(new InputStreamReader(assetManager.open("sql/upgrade/" + filename)));

					while ((line = reader.readLine()) != null) {
						if (line.compareTo("") != 0)
							db.execSQL(line);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}

		onCreate(db);
	}

	public void updateDB(String[] sql) {
		SQLiteDatabase db = getWritableDatabase();

		if (sql != null && sql.length > 0) {
			db.beginTransaction();
			for (String sqlSCommand : sql) {
				db.execSQL(sqlSCommand);
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}

		MyLog.e("DATABASE", "database updated");
	}
}
