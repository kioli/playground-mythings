/** Copyright 2011 Lorenzo Marchiori
 *
 * Class for generating notifications
 *
 * It creates a notification which then stands also when the application finishes and once it is clicked it brings to its own activity
 *
 * Everything useful for it to work is in the package com.kioli.mythings.notification apart for the activities in the manifest, the layouts and strings
 *
 * @author Kioli */

package com.kioli.mythings.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.kioli.mythings.R;

public class NotificationActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		init();
	}

	public void init() {

		findViewById(R.id.button_notification).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				createNotification(v);
			}
		});

		findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void createNotification(View view) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Intent intent = new Intent(this, NotificationReceiver.class);

		// Even if its owning application is killed, the PendingIntent itself will remain usable from other processes
		PendingIntent activity = PendingIntent.getActivity(this, 0, intent, 0);

		Notification notification = new Notification(R.drawable.ic_launcher, getString(R.string.incoming_notification), System.currentTimeMillis());

		// Hide the notification after its selected
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Defines the appearance of the notification once shown
		notification.setLatestEventInfo(this, getString(R.string.notification_title), getString(R.string.notification_text), activity);
		notification.number += 1;
		notificationManager.notify(0, notification);
	}
}