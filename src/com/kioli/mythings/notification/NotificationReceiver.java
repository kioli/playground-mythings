/** Copyright 2011 by m2mobi & Lorenzo Marchiori
 *
 * Class that launches the activity referred to the notification
 *
 * @author Lorenzo */

package com.kioli.mythings.notification;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import com.kioli.mythings.R;

public class NotificationReceiver extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notification);
		init();
	}

	public void init() {
		findViewById(R.id.notification_end_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}