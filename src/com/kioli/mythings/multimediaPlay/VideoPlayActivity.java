package com.kioli.mythings.multimediaPlay;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import com.kioli.mythings.utils.constants.Constants;

/** Class that shows a full screen video located as *.3gp in the res/raw/ folder
 *
 * @author Kioli */
public class VideoPlayActivity extends BetterActivity {

	private VideoView vd;
	private int elapsed_time = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);
	}

	@Override
	protected void onStop() {
		super.onStop();
		getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE).edit().putInt(Constants.PREF_VIDEO_ELAPSED, vd.getCurrentPosition())
				.commit();

		// If this is a fragment_activity instead that an activity
		// getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	protected void onResume() {
		super.onResume();
		elapsed_time = getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE).getInt(Constants.PREF_VIDEO_ELAPSED, 0);
		init();

		// If this is a fragment_activity instead that an activity
		// getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
	}

	private void init() {
		vd = (VideoView) findViewById(R.id.video);
		Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.howto);
		vd.setVideoURI(uri);
		vd.setKeepScreenOn(true);
		MediaController mc = new MediaController(this);
		mc.setAnchorView(vd);
		vd.setMediaController(mc);
		vd.requestFocus();

		if (elapsed_time != 0) {
			vd.seekTo(elapsed_time);
		}
		vd.start();
	}
}