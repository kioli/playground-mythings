package com.kioli.mythings.multimediaPlay;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

public class MultimediaActivity extends BetterActivity {

	private MediaPlayer applause_sound;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multimedia);
		init();
	}

	private void init() {
		findViewById(R.id.btn_audio).setOnClickListener(listener);
		findViewById(R.id.btn_video).setOnClickListener(listener);

		applause_sound = MediaPlayer.create(this, R.raw.cheering);
	}

	private final OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.btn_audio:
					applause_sound.start();
					break;
				case R.id.btn_video:
					startActivity(new Intent(MultimediaActivity.this, VideoPlayActivity.class));
					break;
				default:
					break;
			}
		}
	};
}