package com.kioli.mythings.uniqueID;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.constants.Constants;
import com.kioli.mythings.utils.constants.ReturnCodes;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

/** This utility class is used to get the device's wifi MAC address, to be used as a unique identifier for the device. */
public class UniqueIdentifierUtil {

	private String deviceId = null;
	private WifiManager manager;
	private Context context;

	private static final Pattern PATTERN_MAC = Pattern.compile("^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$");
	private Timer timer;
	private final long TIMEOUT = 5000;

	/** Handler that should receive the result */
	private Handler resultHandler;

	/** single static instance of this */
	private static UniqueIdentifierUtil util;

	private UniqueIdentifierUtil(Context ctx) {
		this.context = ctx;
	}

	public static UniqueIdentifierUtil getInstance(Context ctx) {
		if (util == null) {
			return new UniqueIdentifierUtil(ctx);
		} else {
			util.context = ctx;
			return util;
		}
	}

	/** Attempts to fetch the MAC address for the WIFI adapter to use as a unique device identifier.
	 *
	 * @param ctx
	 *        Context used for getting access to SystemServices, etc.
	 * @param handler
	 *        Handler that will be notified once deviceId is known **/
	public void requestDeviceId(final Handler handler) {

		resultHandler = handler;

		try {
			manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

			if (manager.isWifiEnabled()
					|| (manager.getConnectionInfo().getMacAddress().length() > 0 && PATTERN_MAC.matcher(
							manager.getConnectionInfo().getMacAddress().toUpperCase()).matches())) {
				MyLog.e("UniqueIdentifierUtil", "Wifi enabled");
				deviceId = manager.getConnectionInfo().getMacAddress();
				sendDeviceId(ReturnCodes.RESULTOK);
			} else {
				MyLog.e("UniqueIdentifierUtil", "Wifi not enabled");
				context.registerReceiver(wifiReceiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
				manager.setWifiEnabled(true);

				// Discard timer (if exists)
				if (timer != null) {
					timer.cancel();
					timer = null;
				}

				// Make a new one
				if (timer == null) {
					timer = new Timer();
					timer.schedule(new TimerTask() {
						@Override
						public void run() {
							manager.setWifiEnabled(false);
							context.unregisterReceiver(wifiReceiver);
							sendDeviceId(ReturnCodes.NOTFOUND);
						}
					}, TIMEOUT);
				}
			}
		} catch (Exception e) {
			MyLog.e("UniqueIdentifierUtil", "Exception reading wifi MAC: " + e);
		}
	}

	private void sendDeviceId(int result) {
		Message msg = new Message();
		msg.what = result;

		if (result == ReturnCodes.RESULTOK) {
			Bundle data = new Bundle();
			data.putString(Constants.KEY_DEVICEID, deviceId.toUpperCase());
			msg.setData(data);
		}

		resultHandler.sendMessage(msg);
	}

	/** Receiver that will be notified of changes to the WIFI_STATE Will handle getting the WIFI-MAC once it becomes
	 * available. */
	private final BroadcastReceiver wifiReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);

			MyLog.e("UniqueIdentifierUtil", "Wifi state changed: " + extraWifiState);
			switch (extraWifiState) {
				case WifiManager.WIFI_STATE_ENABLED:
					String macAddr = manager.getConnectionInfo().getMacAddress();
					if (macAddr == null || macAddr.length() == 0) {
						deviceId = "";
						manager.setWifiEnabled(false);
						context.unregisterReceiver(wifiReceiver);
						sendDeviceId(ReturnCodes.NOTFOUND);
					} else {
						deviceId = macAddr;
						manager.setWifiEnabled(false);
						context.unregisterReceiver(wifiReceiver);
						sendDeviceId(ReturnCodes.RESULTOK);
					}

					// We have a valid deviceID so stop the timer
					if (timer != null) {
						timer.cancel();
						timer = null;
					}
					break;
				default:
					break;
			}
		}
	};
}