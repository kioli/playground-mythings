package com.kioli.mythings.uniqueID;

import java.lang.ref.WeakReference;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import com.kioli.mythings.utils.constants.Constants;
import com.kioli.mythings.utils.constants.ReturnCodes;

/** Class that gets the MAC address of the phone to uniquely (to a certain degree) identify it
 *
 * @author Kioli */
public class UniqueIDActivity extends BetterActivity {

	private Context context;
	private static TextView txt;
	private static String deviceID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_get_unique_id);
		init();
	}

	public void init() {
		txt = (TextView) findViewById(R.id.txt_unique_id);
		findViewById(R.id.btn_unique_id).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// retrieve deviceId
				UniqueIdentifierUtil.getInstance(context).requestDeviceId(deviceIdHandler);
			}
		});
	}

	/** Handler for receiving an unique deviceID since the phone ID is not unique enough */
	private final DeviceIdHandler deviceIdHandler = new DeviceIdHandler(this);

	static class DeviceIdHandler extends Handler {

		private final WeakReference<UniqueIDActivity> wr;

		public DeviceIdHandler(UniqueIDActivity ref) {
			wr = new WeakReference<UniqueIDActivity>(ref);
		}

		@Override
		public void handleMessage(Message msg) {
			if (wr.get() != null) {
				UniqueIDActivity main = wr.get();
				switch (msg.what) {
					case ReturnCodes.RESULTOK:
						Bundle data = msg.getData();
						String deviceId = data.getString(Constants.KEY_DEVICEID);
						MyLog.i("UniqueIDActivity.DeviceIdHandler", "DeviceId returned: " + deviceId);
						deviceID = deviceId;
						txt.setText(deviceID);
						break;
					case ReturnCodes.NOTFOUND:
						main.finish();
						break;
				}
			}
		}
	}
}