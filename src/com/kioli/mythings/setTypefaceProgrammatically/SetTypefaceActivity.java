package com.kioli.mythings.setTypefaceProgrammatically;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

public class SetTypefaceActivity extends BetterActivity {

	private static TextView txt;
	private int style_counter = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_typeface);
		init();
	}

	private void init() {
		final Typeface chalet = Typeface.createFromAsset(getAssets(), "fonts/chalet_london.ttf");
		final Typeface chalet_bold = Typeface.createFromAsset(getAssets(), "fonts/chalet.otf");
		final Typeface good_dog = Typeface.createFromAsset(getAssets(), "fonts/good_dog.otf");
		final Typeface molot = Typeface.createFromAsset(getAssets(), "fonts/molot.otf");

		txt = (TextView) findViewById(R.id.txt_typeface);

		// Set a typeface to avoid nullPointer exception
		txt.setTypeface(chalet);

		findViewById(R.id.btn_typeface).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (style_counter) {
					case 0:
						txt.setText("font is chalet");
						txt.setTypeface(chalet_bold);
						style_counter++;
						break;
					case 1:
						txt.setText("font is chalet bold");
						txt.setTypeface(chalet_bold);
						style_counter++;
						break;
					case 2:
						txt.setText("font is good dog");
						txt.setTypeface(good_dog);
						style_counter++;
						break;
					case 3:
						txt.setText("font is molot");
						txt.setTypeface(molot);
						style_counter = 0;
						break;
					default:
						break;
				}
			}
		});
	}
}