package com.kioli.mythings.parseHTMLforWebview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.utils.MyLog;
import com.kioli.mythings.utils.betterComponents.BetterActivity;
import com.kioli.mythings.utils.constants.Constants;

/** Display Terms & Conditions from an HTML source */
public class ParseHTMLforWebviewActivity extends BetterActivity {

	/** generic HTML */
	private final static String HEADER1 = "<html><head><style type=\"text/css\">";
	private final static String HEADER2 = "</style></head><body><div class=\"box\">";
	private final static String FOOTER = "</div></body></html>";
	private final static String CSS_ASSET = "statics/stylesheets/terms.css";

	public final static String TERMS_MIMETYPE = "text/html";
	public final static String TERMS_ENCODING = "utf-8";

	private String terms_html;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parse_html_webview);
		init();
	}

	private void init() {

		// Set ittle for the included top bar
		TextView title = (TextView)	findViewById(R.id.titlebar).findViewById(R.id.title_bar_title);
		title.setText(getResources().getString(R.string.parsedHTML_topbar));

		// Start building final html
		StringBuilder htmlBuilder = new StringBuilder();
		htmlBuilder.append(HEADER1);

		// Insert css into head
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(getApplicationContext().getAssets().open(CSS_ASSET)));
			String line;
			while ((line = reader.readLine()) != null) {
				htmlBuilder.append(line);
			}
		} catch (IOException e) {
			MyLog.e("WebView with URL", "Exception when reading the CSS for the webview" + e);
		}

		// Close head, start body
		htmlBuilder.append(HEADER2);

		// Get body from the intent or from a pre-defined string
		terms_html = getIntent().getStringExtra(Constants.KEY_TERMS_AND_CONDITIONS);
		if (terms_html == null) {
			terms_html = getResources().getString(R.string.parsedHTML_html);
		}

		// Attempt to decode HTML text (possibly received from webservice) and add it
		try {
			terms_html = URLDecoder.decode(terms_html, TERMS_ENCODING);
		} catch (Exception e) {
			MyLog.e("WebView with URL", "Exception decoding HTML: " + e);
		} finally {
			htmlBuilder.append(terms_html);
		}

		// Close html
		htmlBuilder.append(FOOTER);

		WebView web = (WebView) findViewById(R.id.parsed_webview);
		web.loadDataWithBaseURL(null, htmlBuilder.toString(), TERMS_MIMETYPE, TERMS_ENCODING, null);
	}
}