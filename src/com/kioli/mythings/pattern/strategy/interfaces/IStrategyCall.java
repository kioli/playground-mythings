package com.kioli.mythings.pattern.strategy.interfaces;

public interface IStrategyCall {
	public String call();
}