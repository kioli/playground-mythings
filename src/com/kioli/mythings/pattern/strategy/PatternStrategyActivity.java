package com.kioli.mythings.pattern.strategy;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.kioli.mythings.R;
import com.kioli.mythings.pattern.strategy.strategyCall.CallMute;
import com.kioli.mythings.pattern.strategy.strategyCall.CallQuack;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

public class PatternStrategyActivity extends BetterActivity implements OnClickListener {

	TextView	textVoice;
	Button		btnChangeStrategy;
	MandarinDuckSubClass myDuck;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
		talk();
	}
	
	private void talk() {
		textVoice.setText(myDuck.performCall());
	}
	
	private void init() {
		setContentView(R.layout.pattern_strategy);
		textVoice = ((TextView) findViewById(R.id.strategy_text_voice));
		btnChangeStrategy = ((Button) findViewById(R.id.strategy_change));
		
		btnChangeStrategy.setOnClickListener(this);

		myDuck = new MandarinDuckSubClass();
	}
	
	@Override
	public void onClick(View v) {
		if (myDuck.getStrategyCall() instanceof CallMute) {
			myDuck.setCallStrategy(new CallQuack());					
		} else {
			myDuck.setCallStrategy(new CallMute());			
		}
		talk();
	}
}