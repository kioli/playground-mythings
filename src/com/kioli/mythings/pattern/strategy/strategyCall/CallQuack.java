package com.kioli.mythings.pattern.strategy.strategyCall;

import com.kioli.mythings.pattern.strategy.interfaces.IStrategyCall;

public class CallQuack implements IStrategyCall{

	@Override
	public String call() {
		return "I am a duck who says Quack";		
	}

}