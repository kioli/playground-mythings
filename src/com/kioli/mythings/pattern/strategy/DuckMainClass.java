package com.kioli.mythings.pattern.strategy;

import com.kioli.mythings.pattern.strategy.interfaces.IStrategyCall;

/**
 * Main class that act as parent class for the numerous child classes that can
 * differ for several behaviours. All these behaviours can be implemented against
 * interfaces (where the needed methods are specified) in different classes
 * 
 * In this example we have ducks classes that implement the interfaces for flying and call behaviours
 * and inherit the other methods from this class
 */
public abstract class DuckMainClass {

	private IStrategyCall strategyCall;

	public void setCallStrategy(IStrategyCall strategy) {
		strategyCall = strategy;
	}
	public IStrategyCall getStrategyCall() {
		return strategyCall;
	}
	
	public abstract String display();
	
	public String swim() {
		return "I can swim";
	}
	
	public String performCall() {
		return strategyCall.call();
	}
}