package com.kioli.mythings.pattern.strategy;

import com.kioli.mythings.pattern.strategy.strategyCall.CallQuack;

public class MandarinDuckSubClass extends DuckMainClass {
		
	public MandarinDuckSubClass() {
		setCallStrategy(new CallQuack());
	}

	@Override
	public String display() {
		return "I look like a Mandarin duck"; 
	}
}