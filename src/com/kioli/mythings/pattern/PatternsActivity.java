package com.kioli.mythings.pattern;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.kioli.mythings.R;
import com.kioli.mythings.pattern.strategy.PatternStrategyActivity;
import com.kioli.mythings.utils.betterComponents.BetterActivity;

public class PatternsActivity extends BetterActivity {

	PatternsAdapter adapter;
	ListView list;
	ArrayList<String> buttons;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		init();
	}

	public interface ICallback {
		public void onItemClicked(int button);
	}
	
	private void init() {
		list = (ListView) findViewById(R.id.main_list);

		buttons = new ArrayList<String>(Arrays.asList(getResources()
				.getStringArray(R.array.patterns_array)));

		adapter = new PatternsAdapter(this, buttons);
		adapter.setCallback(listClickCallback);

		list.setAdapter(adapter);
	}

	ICallback listClickCallback = new ICallback() {
		@Override
		public void onItemClicked(int position) {
			switch (position) {
			case 0:
				startActivity(new Intent(PatternsActivity.this,
						PatternStrategyActivity.class));
				break;
			default:
				break;
			}
		}
	};
}