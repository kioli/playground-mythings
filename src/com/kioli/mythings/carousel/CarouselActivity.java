package com.kioli.mythings.carousel;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kioli.mythings.R;

public class CarouselActivity extends Activity implements OnClickListener {

	// Define the number of items visible when the carousel is first shown
	private static final float INITIAL_ITEMS_COUNT = 2.5F;

	private LinearLayout mCarouselContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_carousel);

		mCarouselContainer = (LinearLayout) findViewById(R.id.carousel);
		initializeCarousel();
	}

	private void initializeCarousel() {
		final WindowManager w = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		final DisplayMetrics m = new DisplayMetrics();
		w.getDefaultDisplay().getMetrics(m);

		getWindowManager().getDefaultDisplay().getMetrics(m);
		final int imageWidth = (int) (m.widthPixels / INITIAL_ITEMS_COUNT);

		final TypedArray resourcesArray = getResources().obtainTypedArray(
				R.array.puppies_array);

		// Populate the carousel with items
		ImageView imageItem;
		for (int i = 0; i < resourcesArray.length(); i++) {
			// Create new ImageView
			imageItem = new ImageView(this);
			imageItem.setBackgroundResource(R.drawable.shadow);
			imageItem.setImageResource(resourcesArray.getResourceId(i, -1));
			imageItem.setLayoutParams(new LinearLayout.LayoutParams(imageWidth,
					imageWidth));
			imageItem.setId(i);
			imageItem.setOnClickListener(this);

			mCarouselContainer.addView(imageItem);
		}
	}

	@Override
	public void onClick(View v) {
		Toast.makeText(this, "click on image " + v.getId(), Toast.LENGTH_SHORT).show();
	}
}