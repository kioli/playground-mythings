package com.kioli.mythings.fragments.frg;

import android.support.v4.app.Fragment;

/** Base class for all Fragments in this package that is used as their super class */
public class MyFragment extends Fragment {

    public interface IActivityFragment extends IFragmentController {
        void onTempInfoUpdated(String info);
    }

    protected IActivityFragment mInfoCallback;

    protected void updateTempInfoToActivity(final String s) {
        if (mInfoCallback != null)
            mInfoCallback.onTempInfoUpdated(s.toString());
    }

    public void setTempInfoListener(final IActivityFragment listener) {
        mInfoCallback = listener;
    }

}