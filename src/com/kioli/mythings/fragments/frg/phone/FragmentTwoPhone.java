package com.kioli.mythings.fragments.frg.phone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.kioli.mythings.R;
import com.kioli.mythings.fragments.frg.FragmentTwo;

public class FragmentTwoPhone extends FragmentTwo implements OnClickListener {

    private View    nextButton;
    private View    backButton;
    private View	clicksButton;
    
    private TextView	count;

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_two, container, false);

        nextButton = view.findViewById(R.id.btn_next);
        backButton = view.findViewById(R.id.btn_back);
        clicksButton = view.findViewById(R.id.btn_add);
        count = (TextView) view.findViewById(R.id.text_count);
        
        nextButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        clicksButton.setOnClickListener(this);
        
        return view;
    }

    /**
     * Method called from the director to fill the fragment_activity with previously saved data
     */
    public void initData(final int numberClicks){
        count.setText(getString(R.string.number_clicks, numberClicks));
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                mController.onBackButtonClicked();
                break;
            case R.id.btn_next:
                mController.onNextButtonClicked();
                break;
            case R.id.btn_add:
                mController.incrementClicks();
            default:
                break;
        }
    }
}