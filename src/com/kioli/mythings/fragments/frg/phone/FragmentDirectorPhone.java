package com.kioli.mythings.fragments.frg.phone;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import com.kioli.mythings.R;
import com.kioli.mythings.fragments.frg.FRAGM;
import com.kioli.mythings.fragments.frg.FragmentDialog;
import com.kioli.mythings.fragments.frg.FragmentOne;
import com.kioli.mythings.fragments.frg.FragmentThree;
import com.kioli.mythings.fragments.frg.FragmentTwo;
import com.kioli.mythings.fragments.frg.MyFragmentDirector;
import com.kioli.mythings.utils.ParcelableHelper;

public class FragmentDirectorPhone extends MyFragmentDirector implements FragmentOne.IFragmentOneController, FragmentTwo.IFragmentTwoController, FragmentThree.IFragmentThreeController,
		FragmentDialog.IFragmentDialogController {

	protected IActivityController	mGPActivity;
	private final FragmentManager	mFrman;
	private State					mState;

	public FragmentDirectorPhone(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		inflate(context, R.layout.activity_fragment_director, this);

        mState = new State();
		mFrman = getFragmentManager();

		/** Control if the director has been called from an Activity implementing its interface for mutual communication */
		if ((context instanceof IActivityController) == false) {
			throw new ClassCastException("activity must implement IController");
		}
		mGPActivity = (IActivityController) context;
	}

    @Override
    public boolean isStarted() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
	public void onResume() {
		final FRAGM fragm = mState.mCurFragment;
		if (fragm != null) {
			initData(fragm);
		}
	}

	/** Method that fills the fragment_activity with the proper data stored previously
	 * 
	 * @param fragm
	 *        The fragment_activity currently displayed */
	private void initData(final FRAGM fragm) {
		switch (fragm) {
			case FRAGMENT_ONE:
				final FragmentOnePhone frgOne = (FragmentOnePhone) mFrman.findFragmentByTag(fragm.name());
				break;
			case FRAGMENT_TWO:
				final FragmentTwoPhone frgTwo = (FragmentTwoPhone) mFrman.findFragmentByTag(fragm.name());
				frgTwo.initData(mGPActivity.getNumberOfClicksFromActivity());
				break;
			case FRAGMENT_THREE:
				final FragmentThreePhone frgThree = (FragmentThreePhone) mFrman.findFragmentByTag(fragm.name());
				break;
            case FRAGMENT_DIALOG:
                final FragmentDialogPhone fgmDialog = (FragmentDialogPhone) mFrman.findFragmentByTag(fragm.name());
                String content =  mGPActivity.getDialogContent();
                fgmDialog.initData(content);
                break;
			default:
				break;
		}
	}

	/** Method called by the activity to tell the director what to do when the back button is pressed */
	@Override
	public boolean goBack() {
		switch (mState.mCurFragment) {
			case FRAGMENT_ONE:
				mGPActivity.onBackLastFragment();
				return false;
			case FRAGMENT_TWO:
				switchToFragment(FRAGM.FRAGMENT_ONE);
				return true;
			case FRAGMENT_THREE:
				switchToFragment(FRAGM.FRAGMENT_TWO);
				return true;
			default:
				return false;
		}
	}

	@Override
	public void forwardWithFragment(final FRAGM fragm) {
		switchToFragment(fragm);
	}

	/** Method that switches the fragment_activity on the screen
	 * 
	 * @param fragm
	 *        The fragment_activity that need to be shown */
	public void switchToFragment(final FRAGM fragm) {
		final FragmentTransaction tr = mFrman.beginTransaction();
		final String tag = fragm.name();

		// Perform fragment_activity swap
		switch (fragm) {
			case FRAGMENT_ONE:
				tr.replace(R.id.frame, new FragmentOnePhone(), tag);
				tr.commitAllowingStateLoss(); // This is used instead of tr.commit(), as otherwise it might make the app
				// crash when a pop-up is shown while being in the Log-in screen.
				break;
			case FRAGMENT_TWO:
				tr.replace(R.id.frame, new FragmentTwoPhone(), tag);
				tr.commitAllowingStateLoss(); // This is used instead of tr.commit(), as otherwise it might make the app
				// crash when a pop-up is shown while being in the Log-in screen.
				break;
			case FRAGMENT_THREE:
				tr.replace(R.id.frame, new FragmentThreePhone(), tag);
				tr.commitAllowingStateLoss(); // This is used instead of tr.commit(), as otherwise it might make the app
				// crash when a pop-up is shown while being in the Log-in screen.
				break;
			case FRAGMENT_DIALOG:
				final FragmentDialogPhone dialog = new FragmentDialogPhone();
                dialog.setCancelable(false);
				dialog.show(mFrman, tag);
				break;
			default:
				break;
		}

		if (mFrman.executePendingTransactions()) {
			mState.mCurFragment = fragm;
			initData(fragm);
		}
	}

	public void closeFragmentDialog() {
		if (mState.mCurFragment.name().equals(FRAGM.FRAGMENT_DIALOG.name())) {
			final FragmentDialog dialog = (FragmentDialog) mFrman.findFragmentByTag(mState.mCurFragment.name());
			dialog.dismiss();
			mState.mCurFragment = FRAGM.FRAGMENT_THREE;
		}
	}

	@Override
	public State onSaveInstanceState() {
		mState.childrenState = super.onSaveInstanceState();
		return mState;
	}

	@Override
	public void onRestoreInstanceState(final Parcelable savedState) {
		mState = (State) savedState;
		super.onRestoreInstanceState(mState.childrenState);
	}

    @Override
    public void showDialog() {
        switchToFragment(FRAGM.FRAGMENT_DIALOG);
    }

    @Override
    public void onTempInfoUpdated(String info) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onNextButtonClicked() {
    	switch (mState.mCurFragment) {
            case FRAGMENT_ONE:
                switchToFragment(FRAGM.FRAGMENT_TWO);
                break;
            case FRAGMENT_TWO:
                switchToFragment(FRAGM.FRAGMENT_THREE);
                break;
            default:
            }
    }

    @Override
    public void onBackButtonClicked() {
    	switch (mState.mCurFragment) {
            case FRAGMENT_ONE:
                mGPActivity.onBackLastFragment();
                break;
            case FRAGMENT_TWO:
                switchToFragment(FRAGM.FRAGMENT_ONE);
                break;
            case FRAGMENT_THREE:
                switchToFragment(FRAGM.FRAGMENT_TWO);
                break;
            case FRAGMENT_DIALOG:
                switchToFragment(FRAGM.FRAGMENT_THREE);
                break;
            default:
            	break;
            }
    }

    @Override
    public void incrementClicks() {
        mGPActivity.saveUserClicks();
        ((FragmentTwoPhone) mFrman.findFragmentByTag(mState.mCurFragment.name())).initData(mGPActivity.getNumberOfClicksFromActivity());
    }

    @Override
    public void handleDialogResult(String contentDialog) {
        mGPActivity.setDialogContent(contentDialog);
        closeFragmentDialog();
    }

    private static final class State implements Parcelable {
		public FRAGM		mCurFragment;
		public Parcelable	childrenState;

		public State() {
		}

		public State(final Parcel in) {
			mCurFragment = ParcelableHelper.readEnum(in, FRAGM.class);
			childrenState = in.readParcelable(null);
		}

		@Override
		public void writeToParcel(final Parcel out, final int flags) {
			ParcelableHelper.writeEnum(out, mCurFragment);
			out.writeParcelable(childrenState, flags);
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@SuppressWarnings("unused")
		// used by android
		public static final Creator<State>	CREATOR	= new Creator<State>() {
														@Override
														public State[] newArray(final int size) {
															return null;
														}

														@Override
														public State createFromParcel(final Parcel in) {
															return new State(in);
														}
													};
	}
}