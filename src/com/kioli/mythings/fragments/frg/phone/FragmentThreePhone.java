package com.kioli.mythings.fragments.frg.phone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.kioli.mythings.R;
import com.kioli.mythings.fragments.frg.FragmentThree;

public class FragmentThreePhone extends FragmentThree implements OnClickListener {

    private View    dialogButton;
    private View    backButton;

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_three, container, false);

        dialogButton = view.findViewById(R.id.btn_dialog);
        backButton = view.findViewById(R.id.btn_back);

        dialogButton.setOnClickListener(this);
        backButton.setOnClickListener(this);

        return view;
    }

    /**
     * Method called from the director to fill the fragment_activity with previously saved data
     */
    public void initData(){
		// Nothing specific here
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                mController.onBackButtonClicked();
                break;
            case R.id.btn_dialog:
                mController.showDialog();
                break;
            default:
                break;
        }
    }
}