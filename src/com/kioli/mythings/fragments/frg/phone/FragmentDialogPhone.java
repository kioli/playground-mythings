package com.kioli.mythings.fragments.frg.phone;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.kioli.mythings.R;
import com.kioli.mythings.fragments.frg.FragmentDialog;

public class FragmentDialogPhone extends FragmentDialog implements OnClickListener {

    private enum CURRENT_FOCUSED_ITEM {
        UNKNOWN, NAME_FIELD
    };

    private Button btnDone;

    private EditText nameInput;

    private CURRENT_FOCUSED_ITEM currentFocusedView = CURRENT_FOCUSED_ITEM.UNKNOWN;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // This is necessary not to display the ugly black/white Window background
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_Translucent_Modified_GP);
        this.setCancelable(false);
    }

    @Override
    public void onPause() {
        saveCurrentFocusState();
        super.onPause();
    }

    @Override
    public void onResume() {
        resumeCurrentFocusState();
        super.onResume();
    }

    // Method to save which of the input fields has focus, so when we Pause-> Resume the activity, the keyboard shows in
    // the right input field
    protected void saveCurrentFocusState() {
        if (nameInput.hasFocus()) {
            currentFocusedView = CURRENT_FOCUSED_ITEM.NAME_FIELD;
        } else {
            currentFocusedView = CURRENT_FOCUSED_ITEM.UNKNOWN;
        }
    }

    // Complementary to saveCurrentFocusState. 
    // If the focus is saved to any of the inputs, we trigger the keyboard on that input
    protected void resumeCurrentFocusState() {
        if (currentFocusedView == CURRENT_FOCUSED_ITEM.NAME_FIELD) {
            nameInput.requestFocus();
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog, container, false);

        btnDone = (Button) view.findViewById(R.id.btn_dismiss_dialog);
        btnDone.setOnClickListener(this);

        nameInput = (EditText) view.findViewById(R.id.edit_text_dialog);

        return view;
    }

    public void initData(final String name) {
        nameInput.setText(name);
        nameInput.setSelection(nameInput.getText().length());
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
        case R.id.btn_dismiss_dialog:
            mController.handleDialogResult(nameInput.getText().toString());
            break;
        default:
            break;
        }
    }
}