package com.kioli.mythings.fragments.frg;

import android.app.Activity;

import com.kioli.mythings.fragments.ActivityFragment;
import com.kioli.mythings.utils.FragmentDirector;

abstract public class FragmentThree extends MyFragment {

    /** interface to allow the fragment_activity to communicate with the activity {@link ActivityFragment} */
    public interface IFragmentThreeController extends IFragmentController {
        public void showDialog();
    }

    protected IFragmentThreeController mController;

    /**
     * checks if the activity holding the fragment_activity implements its interface and automatically sets the object to allow
     * the fragment_activity to communicate with the activity
     */
    @Override
    public void onAttach(final Activity activity) {
        final FragmentDirector dir = ((ActivityFragment) activity).getmDirector();

        // Dir != null, since when Android destroys our activity, when resuming the Fragments the Director is still NOT present
        if (dir != null) {
            if ((dir instanceof IFragmentThreeController) == false)
                throw new ClassCastException("activity must implement IController");
            super.onAttach(activity);
            mController = (IFragmentThreeController) dir;
        } else {
            mController = null;
            super.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        mController = null;
        super.onDetach();
    }
}