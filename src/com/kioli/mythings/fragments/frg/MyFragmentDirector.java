package com.kioli.mythings.fragments.frg;

import android.content.Context;
import android.util.AttributeSet;
import com.kioli.mythings.fragments.ActivityFragment;
import com.kioli.mythings.fragments.frg.MyFragment.IActivityFragment;
import com.kioli.mythings.utils.FragmentDirector;

public abstract class MyFragmentDirector extends FragmentDirector<FRAGM, ActivityFragment> implements
        IActivityFragment {

    public interface IActivityController {
        void onBackLastFragment();
        void setDialogContent(String contentDialog);
        void saveUserClicks();
        String getDialogContent();
		int getNumberOfClicksFromActivity();
    }

    public MyFragmentDirector(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    abstract public boolean isStarted();
}