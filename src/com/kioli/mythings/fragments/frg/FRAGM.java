package com.kioli.mythings.fragments.frg;

public enum FRAGM {
    FRAGMENT_ONE,
    FRAGMENT_TWO,
    FRAGMENT_THREE,
    FRAGMENT_DIALOG
}