package com.kioli.mythings.fragments.frg;

/**
 * Interface base implemented by other interfaces in {@link MyFragment}, {@Link FragmentOne},
 * {@Link FragmentTwo}, {@Link FragmentThree}
 */
public interface IFragmentController {
    void onNextButtonClicked();
    void onBackButtonClicked();
}