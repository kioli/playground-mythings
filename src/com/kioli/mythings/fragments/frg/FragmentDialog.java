package com.kioli.mythings.fragments.frg;

import android.app.Activity;
import android.support.v4.app.DialogFragment;

import com.kioli.mythings.fragments.ActivityFragment;
import com.kioli.mythings.utils.FragmentDirector;

abstract public class FragmentDialog extends DialogFragment {

    /** interface to allow the fragment_activity to communicate with the activity {@link com.kioli.mythings.fragments.ActivityFragment} */
    public interface IFragmentDialogController extends IFragmentController {
        void handleDialogResult(String contentDialog);
    }

    protected IFragmentDialogController mController;

    /**
     * checks if the activity holding the fragment_activity implements its interface and automatically sets the object to allow
     * the fragment_activity to communicate with the activity
     */
    @Override
    public void onAttach(final Activity activity) {
        final FragmentDirector dir = ((ActivityFragment) activity).getmDirector();

        // Dir != null, since when Android destroys our activity, when resuming the Fragments the Director is still NOT present
        if (dir != null) {
            if ((dir instanceof IFragmentDialogController) == false)
                throw new ClassCastException("activity must implement IController");
            super.onAttach(activity);
            mController = (IFragmentDialogController) dir;
        } else {
            mController = null;
            super.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        mController = null;
        super.onDetach();
    }
}