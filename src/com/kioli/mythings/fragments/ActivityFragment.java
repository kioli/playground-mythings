package com.kioli.mythings.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.kioli.mythings.R;
import com.kioli.mythings.fragments.frg.FRAGM;
import com.kioli.mythings.fragments.frg.FragmentDialog;
import com.kioli.mythings.fragments.frg.MyFragmentDirector;

public class ActivityFragment extends FragmentActivity implements MyFragmentDirector.IActivityController {

	private MyFragmentDirector	mDirector;
	private String				dialogContent;
	private int					numberOfCLicks;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragments);

		mDirector = (MyFragmentDirector) findViewById(R.id.director);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mDirector.onResume();
		if (mDirector.isStarted() == false) {
			mDirector.forwardWithFragment(FRAGM.FRAGMENT_ONE);
		}
	}

	public MyFragmentDirector getmDirector() {
		return mDirector;
	}

	@Override
	public void onBackPressed() {
		mDirector.goBack();
	}

	@Override
	public void onBackLastFragment() {
		showExitMessage();
	}

	@Override
	public void setDialogContent(String contentDialog) {
		this.dialogContent = contentDialog;
	}
	
	@Override
	public void saveUserClicks() {
		numberOfCLicks++;
	}

	@Override
	public int getNumberOfClicksFromActivity() {
		return numberOfCLicks;
	}
	
	@Override
	public String getDialogContent() {
		return dialogContent;
	}

	private void showExitMessage() {
		FragmentManager fm = getSupportFragmentManager();
		FragmentDialog myDialog = new FragmentDialog() {
			
			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {
				// Use the Builder class for convenient dialog construction
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setMessage("Do you want to exit?").setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						ActivityFragment.this.finish();
					}
				}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						getDialog().dismiss();
					}
				});
				// Create the AlertDialog object and return it
				return builder.create();
			}
		};
		myDialog.setCancelable(false);
		myDialog.show(fm, "fragment_out");
	}
}